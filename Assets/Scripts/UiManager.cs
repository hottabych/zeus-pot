﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UiManager : MonoBehaviour
{
    public Canvas canvas;

    public GameObject blur;

    public Slider healthbar;
    public Text enemyCounterText;
    public Text shotsText;
    public RectTransform radar; //ссылка на радар в сцене
    public GameObject radarTarget; //префаб цели на радаре
    Stack<GameObject> targets = new Stack<GameObject>();
    
    public GameObject tabletPopup;
    public GameObject greetingsRadio;
    public GameObject radioRGDialog;

    public Transform ammoLayout;
    public GameObject bulletIconPrefab;
    Stack<GameObject> bulletIcons = new Stack<GameObject>(10);
    //TODO: добавить публичное свойство-геттер к Stack.Count вместо переменной Player.shots

    public Image cupImage;

    public Sprite bronzeCup;
    public Sprite silverCup;
    public Sprite goldCup;

    public void RefillAmmo()
    {
        for (int i = 0; i < 10; i++)
        {
            var newBullet = Instantiate(bulletIconPrefab, ammoLayout);
            //newBullet.transform.localScale *= canvas.scaleFactor;
            bulletIcons.Push(newBullet);
        }
    }


    //вызывается из Player.Shoot()
    public void DecrementAmmo()
    {
        if (bulletIcons.Count == 0)
        {
            Debug.LogError("Ammo stack is empty, but you try to pop", transform);
            return;
        }

        var iconToDelete = bulletIcons.Pop();
        Destroy(iconToDelete);
    }

    private void InitRadar()
    {
        AddTargetsToRadar(GameManager.instance.enemiesTotal);
    }


    //вызывается из Enemy
    public void DeleteTargetFromRadar()
    {
        if (targets.Count == 0) return;
        
        GameObject t = targets.Pop();
        Destroy(t);
    }

    public void AddTargetsToRadar(int count)
    {
        for (int i = 0; i < count; i++)
        {
            Vector2 randomPos = (Vector2)radar.transform.position + canvas.scaleFactor * Random.insideUnitCircle * radar.sizeDelta.x / 2f;
            GameObject t = Instantiate(radarTarget, randomPos, Quaternion.identity);
            t.transform.localScale *= canvas.scaleFactor;
            t.transform.SetParent(radar);
            targets.Push(t);
        }
    }

    private void Start()
    {
        InitRadar();
    }
}
