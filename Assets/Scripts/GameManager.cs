﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using FlipWebApps.BeautifulTransitions.Scripts.Transitions;
using System;
using Random = UnityEngine.Random;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static bool gamePaused;

    public Player player;
    [HideInInspector]
    public UiManager ui;    

    bool gameover = false;

    [HideInInspector]
    public bool levelCompleted = false;    

    //для диалогов с выбором буквы
    public enum TabletType { None, NoBullets, NoHealth}
    TabletType tablet = TabletType.None;

    [HideInInspector]
    public Vector2 mouseLastClicked; //координаты последнего щелчка, изменяются из Enemy

    public int healthCritical = 30;     

    public ParticleSystem[] confettiEmitters;

    //номер правильного ответа на вопроса, если кончились патроны / здоровье
    //каждый раз генерируется случайно
    private int correctAnswer; //0, 1 или 2

    private List<int> initialAnswers = new List<int>(new[] { 0, 1, 2 });  //в начале фиксированный набор случайных вопросов


    [Header("Spawn Enemies")]
    public EnemySpawner[] enemySpawners; //правый и левый

    public int enemiesTotal = 50; //общий остаток врагов
    [HideInInspector]
    public int enemiesActive; //сейчас на экране
    public int enemiesToSpawn = 1; //сколько спаунить за волну (увеличивается от 1 до 4)    

    public float spawnDelay = .5f;
    public float resumeEnemiesDelay = 3.2f; //оживание после закрытия планшета
    bool canSpawn = false;


    [Space(10f)]
    [Header("Audio")]

    [HideInInspector]
    public new AudioSource audio;

    public float musicVolumeDef = .1f;
    public float musicVolumeDuck = .02f;

    public AudioSource musicSource;


    public AudioClip theyAreComingClip;
    public AudioClip youMissedClip;
    public AudioClip greatContinueClip;
    public AudioClip anotherAlienDestroyedClip;
    public AudioClip shootQuickerClip;
    public AudioClip invasionPreventedClip;
    public AudioClip congratulationsClip;
    public AudioClip noMoreAmmoTapAClip; //16
    public AudioClip noMoreAmmoTapBClip;
    public AudioClip noMoreAmmoTapCClip;

    public AudioClip noMoreHealthTapAClip; //25
    public AudioClip noMoreHealthTapBClip;
    public AudioClip noMoreHealthTapCClip;

    public AudioClip ammoFullClip; //22
    public AudioClip healthFullClip; //31   

    public AudioClip wrongButtonBulletsTapAClip; //17
    public AudioClip wrongButtonBulletsTapBClip;
    public AudioClip wrongButtonBulletsTapCClip;

    public AudioClip wrongButtonHealthTapAClip; //26
    public AudioClip wrongButtonHealthTapBClip;
    public AudioClip wrongButtonHealthTapCClip;


    private void Awake()
    {
        instance = this;

        ui = GetComponent<UiManager>();
        audio = GetComponent<AudioSource>();
        mouseLastClicked = new Vector2 (Screen.width, 0f); //так как герой по умолчанию повернут вправо

        initialAnswers.Shuffle();        
    }

    private void Start()
    {
        audio.PlayOneShot(theyAreComingClip);

        //профилактика на случай "зомби"
        InvokeRepeating("CleanEnemies", 1f, 1f);
    }

    private void Update()
    {
        //уничтожили волну - можно запускать новых
        if (enemiesActive == 0) canSpawn = true;

        //в зависимости от уровня выкидываем разное количество врагов
        if (enemiesTotal > 40)
        {
            enemiesToSpawn = 1;
        }
        else if (enemiesTotal > 30)
        {
            enemiesToSpawn = 2;
        }
        else if (enemiesTotal > 20)
        {
            enemiesToSpawn = 3;
        }
        else if (enemiesTotal > 0)
        {
            enemiesToSpawn = 4;
        }

        //TODO: вместо кучи if-ов выше, можно переделать через словарь
        //key: количество врагов, value: сколько спаунить


        //если осталось мало врагов, заспаунить остаток
        if (enemiesTotal < enemiesToSpawn)
        {
            enemiesToSpawn = enemiesTotal;
        }

        //спаун
        if (enemiesTotal > 0 && canSpawn)
        {
            canSpawn = false;
            StartCoroutine(SpawnWave());
        }


        //уровень пройден, если во всех спаунерах осталось 0 врагов            
        if (enemiesTotal == 0 && !levelCompleted)
        {
            WinLevel();
        }

        //кончилось здоровье или патроны
        if (!gamePaused && !levelCompleted)
        {
            if (player.health <= healthCritical)
            {
                tablet = TabletType.NoHealth;
            }
            if (player.shots <= 0)
            {
                tablet = TabletType.NoBullets;
            }
            #region Debug
            if (Input.GetKeyDown(KeyCode.B))
            {
                tablet = TabletType.NoBullets;
            }
            if (Input.GetKeyDown(KeyCode.H))
            {
                tablet = TabletType.NoHealth;
            }
            #endregion
            if (tablet != TabletType.None) PopupTablet();
        }
    }

    //открытие диалога c буквами - закрытие в методе TabletAnswerHandler
    public void PopupTablet()
    {
        ui.blur.SetActive(true);
        musicSource.volume = musicVolumeDuck;

        //вначале спросить все три буквы, далее случайно
        if (initialAnswers.Count > 0)
        {
            correctAnswer = initialAnswers[0];
            initialAnswers.RemoveAt(0);
        }
        else correctAnswer = Random.Range(0, 3);

        if (tablet == TabletType.NoBullets)
        {
            switch (correctAnswer)
            {
                case 0:
                    PlayAudio(noMoreAmmoTapAClip);
                    break;

                case 1:
                    PlayAudio(noMoreAmmoTapBClip);
                    break;

                case 2:
                    PlayAudio(noMoreAmmoTapCClip);
                    break;
            }
        }

        if (tablet == TabletType.NoHealth)
        {
            switch (correctAnswer)
            {
                case 0:
                    PlayAudio(noMoreHealthTapAClip);
                    break;

                case 1:
                    PlayAudio(noMoreHealthTapBClip);
                    break;

                case 2:
                    PlayAudio(noMoreHealthTapCClip);
                    break;
            }
        }


        ui.tabletPopup.SetActive(true);
        LevelStatisticsCounter.instance.levelTryStats.radioAppeared++;

        gamePaused = true;

        PauseAllEnemies();
        DeleteEnemyBullets();

        System.GC.Collect();
    }

    void PauseAllEnemies()
    {
        var activeEnemiesArray = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in activeEnemiesArray)
        {
            var enemyScript = enemy.GetComponent<Enemy>();            
            if (enemyScript.dying) continue; //если враг в процессе гибели, не приводить его в состояние idle
            try
            {
                enemyScript.SetAnimation(enemyScript.idleAnimation, true);
            }
            catch (NullReferenceException)      //непонятная ошибка, появляется иногда при убийстве врага в момент появления планшета
            {
                Debug.LogError("Caught null-ref error in Enemy script");
            }
        }
    }


    IEnumerator ResumeAllEnemies(float delay)
    {
        yield return new WaitForSeconds(delay);

        var activeEnemiesArray = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in activeEnemiesArray)
        {
            yield return new WaitForSeconds(Random.Range(0f, 1f)); //чтобы не начали стрелять одновременно
            if (enemy)  //возможна редкая ситуация, когда враг уничтожен, а мы пытаемся получить к нему доступ
            {
                var enemyScript = enemy.GetComponent<Enemy>();
                if (enemyScript.dying) continue;
                enemyScript.SetAnimation(enemyScript.shotAnimation, true);
            }
        }
    }


    void DeleteEnemyBullets()
    {
        var bullets = GameObject.FindGameObjectsWithTag("Bullet");
        foreach (var item in bullets)
        {
            Destroy(item);
        }
    }


    void WinLevel()
    {
        levelCompleted = true; //блокировка повторных заходов в эту ветвь кода     

        foreach (var ps in confettiEmitters)
        {
            ps.Play();
        }

        if (LevelStatisticsCounter.instance.levelTryStats.wrongAnswersNum == 0)
        {
            ui.cupImage.sprite = ui.goldCup;
            LevelStatisticsCounter.instance.levelStats.cupType = LevelStats.CupType.Gold;
        }
        else if (LevelStatisticsCounter.instance.levelTryStats.wrongAnswersNum > 0 &&
                 LevelStatisticsCounter.instance.levelTryStats.wrongAnswersNum < 5)
        {
            ui.cupImage.sprite = ui.silverCup;
            LevelStatisticsCounter.instance.levelStats.cupType = LevelStats.CupType.Silver;
        }
        else
        {
            ui.cupImage.sprite = ui.bronzeCup;
            LevelStatisticsCounter.instance.levelStats.cupType = LevelStats.CupType.Bronze;
        }

        ui.cupImage.gameObject.SetActive(true);
        TransitionHelper.TransitionIn(ui.cupImage.gameObject);

        LevelStatisticsCounter.instance.levelTryStats.win = true;
        LevelStatisticsCounter.instance.Save(); //сохранение!   

        DeleteEnemyBullets();

        PlayAudio(invasionPreventedClip);
        // ui.greetingsRadio.SetActive(true);
        StartCoroutine(TapMap());
    }


    /// <summary>
    /// ищет "зомби" и уничтожает их
    /// </summary>
    void CleanEnemies()
    {
        var activeEnemiesArray = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (var enemy in activeEnemiesArray)
        {
            if (enemy.GetComponent<Collider2D>().enabled == false)
            {
                var enemyScript = enemy.GetComponent<Enemy>();
                if (enemyScript.GetCurrentStateName() != enemyScript.dieAnimation)
                {
                    Destroy(enemy);
                }
            }
        }
    }


    IEnumerator SpawnWave()
    {
        for (int i = 0; i < enemiesToSpawn; i++)
        {
            int rndSpawner = Random.Range(0, enemySpawners.Length);
            enemySpawners[rndSpawner].Spawn();
            enemiesActive++;

            yield return new WaitForSeconds(spawnDelay);
        }
    }


    IEnumerator TapMap()
    {
        yield return new WaitForSeconds(5f);

        PlayAudio(congratulationsClip);

        yield return new WaitForSeconds(8f);

        SceneManager.LoadScene("map");
    }


    //вызывается из Enemy и здесь
    public void PlayAudio(AudioClip clip, bool interruptCurrent = true)
    {      
        if (interruptCurrent)
        {
            audio.Stop();
        }
        else
        {
            if (audio.isPlaying) return;
        }
        audio.PlayOneShot(clip);
    }


    #region Handlers

    //закрытие диалога c буквами
    public void TabletAnswerHandler(int answer)
    {
        //правильный ответ:
        if (answer == correctAnswer)
        {
            LevelStatisticsCounter.instance.levelTryStats.correctAnswersNum++;
            switch (correctAnswer)
            {
                case 0:
                    LevelStatisticsCounter.instance.levelTryStats.correctAnswerANum++;
                    break;

                case 1:
                    LevelStatisticsCounter.instance.levelTryStats.correctAnswerBNum++;
                    break;

                case 2:
                    LevelStatisticsCounter.instance.levelTryStats.correctAnswerCNum++;
                    break;
            }

            if (tablet == TabletType.NoBullets)
            {
                ui.RefillAmmo();
                PlayAudio(ammoFullClip);
                player.Reload();
            }

            if (tablet == TabletType.NoHealth)
            {
                player.health = 100;
                ui.healthbar.value = 1;
                PlayAudio(healthFullClip);
            }

            tablet = TabletType.None;

            ui.blur.SetActive(false);
            musicSource.volume = musicVolumeDef;
            ui.tabletPopup.SetActive(false);
            gamePaused = false;

            StartCoroutine(ResumeAllEnemies(resumeEnemiesDelay));
        }
        //неправильный ответ:
        else
        {
            LevelStatisticsCounter.instance.levelTryStats.wrongAnswersNum++;
            enemiesTotal += 1;
            ui.AddTargetsToRadar(1);

            if (tablet == TabletType.NoBullets)
            {
                switch (correctAnswer)
                {
                    case 0:
                        PlayAudio(wrongButtonBulletsTapAClip);
                        LevelStatisticsCounter.instance.levelTryStats.wrongAnswerANum++;
                        break;

                    case 1:
                        PlayAudio(wrongButtonBulletsTapBClip);
                        LevelStatisticsCounter.instance.levelTryStats.wrongAnswerBNum++;
                        break;

                    case 2:
                        PlayAudio(wrongButtonBulletsTapCClip);
                        LevelStatisticsCounter.instance.levelTryStats.wrongAnswerCNum++;
                        break;
                }
            }

            if (tablet == TabletType.NoHealth)
            {
                switch (correctAnswer)
                {
                    case 0:
                        PlayAudio(wrongButtonHealthTapAClip);
                        LevelStatisticsCounter.instance.levelTryStats.wrongAnswerANum++;
                        break;

                    case 1:
                        PlayAudio(wrongButtonHealthTapBClip);
                        LevelStatisticsCounter.instance.levelTryStats.wrongAnswerBNum++;
                        break;

                    case 2:
                        PlayAudio(wrongButtonHealthTapCClip);
                        LevelStatisticsCounter.instance.levelTryStats.wrongAnswerCNum++;
                        break;
                }
            }

        }
    }


    public void RedAnswer()
    {
        //UNDONE радио в конце (либо активировать его уже на карте)
    }

    public void GreenAnswer()
    {

    }

    public void ClickMiss()
    {        
        PlayAudio(youMissedClip, false);
        player.Invoke("ShootMiss", .2f);

        //TODO убрать костыли с invoke'ами тут и в EnemyTutorial
        //сделать обобщенный обработчик Enemy и EnemyTutorial
    }

#endregion
}
