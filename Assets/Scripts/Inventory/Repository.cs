﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fox3D.ZeusPot.Market;

namespace Fox3D.ZeusPot.Inventory
{
	public sealed class Repository
	{
		private const string FILE_NAME = "inventory.dat";

		private static Repository _instance;

		public static Repository Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				if (!ES2.Exists(FILE_NAME))
					return _instance = new Repository { _ownedProducts = new HashSet<string>(ProductsList.Instance.PreownedProducts.Select(x => x.Key)) } ;

				return _instance = new Repository { _ownedProducts = new HashSet<string>(ES2.LoadArray<string>(FILE_NAME)) };
			}
		}

		private Repository() { }

		private HashSet<string> _ownedProducts = new HashSet<string>();

		public IEnumerable<AbstractProduct> OwnedProducts
		{
			get
			{
				return _ownedProducts.Select(productKey => ProductsList.Instance.Products.FirstOrDefault(x => x.Key == productKey));
			}
		}

		public void AddOwned(AbstractProduct product)
		{
			if (!ProductsList.Instance.Products.Contains(product))
				throw new InvalidOperationException("Provided product not found in products list.");

			if (_ownedProducts.Contains(product.Key))
				return;

			_ownedProducts.Add(product.Key);

			ES2.Save(_ownedProducts.ToArray(), FILE_NAME);
		}

		public bool IsOwned(AbstractProduct product)
		{
			return _ownedProducts.Contains(product.Key);
		}
	}
}