﻿using System;
using Fox3D.ZeusPot.Market;
using PlasticBlock.UI.Windows;
using UnityEngine;
using UnityEngine.UI;

namespace Fox3D.ZeusPot.Inventory.UI
{
	/// <summary>
	/// UI element of item from inventory.
	/// </summary>
	public sealed class InventoryElement : WindowElementBase
	{
		[SerializeField]
		private Image _icon;

		[SerializeField]
		private TitledButton _selectButton;
		
		[SerializeField]
		private Image _indicator;
		
		private AbstractProduct _product;

		private void Awake()
		{
			if (_icon == null || _selectButton == null || _indicator == null)
				throw new NullReferenceException();
		}

		/// <inheritdoc />
		public override void OnOpen(params object[] args)
		{
			var product = (AbstractProduct) args[0];

			if (product == null)
				throw new NullReferenceException();

			_icon.sprite = product.Icon;
			_icon.preserveAspect = true;
			_selectButton.Self.onClick = new Button.ButtonClickedEvent();
			_selectButton.Self.onClick.AddListener(SelectButtonBehaviour);

			_product = product;

			ChangeStatus(SelectedItems.Instance.SelectedProducts.Contains(_product));

			SelectedItems.Instance.OnProductSelect += SelectionUpdate;
		}

		/// <inheritdoc />
		public override void OnClose()
		{
			SelectedItems.Instance.OnProductSelect -= SelectionUpdate;

			Destroy(gameObject);
		}

		private void SelectButtonBehaviour()
		{
			SelectedItems.Instance.Select(_product);

			ChangeStatus(true);
		}

		private void SelectionUpdate(AbstractProduct product)
		{
			if (product.GetType() != _product.GetType())
				return;
			
			if (product == _product)
				return;

			ChangeStatus(false);
		}

		private void ChangeStatus(bool status)
		{
			_selectButton.Self.interactable = !status;
			_selectButton.Title.text = status ? "Selected" : "Select";
			_indicator.enabled = status;
		}
	}
}