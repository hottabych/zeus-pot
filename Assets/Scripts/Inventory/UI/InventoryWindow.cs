﻿using PlasticBlock.UI.Windows;
using UnityEngine;

namespace Fox3D.ZeusPot.Inventory.UI
{
	public sealed class InventoryWindow : CompoundWindowBase
	{
		[SerializeField]
		private InventoryElement _inventoryElementSample;

		[SerializeField]
		private RectTransform _layout;

		private bool _isOpened;

		/// <inheritdoc />
		public override void Open(params object[] args)
		{
			base.Open(args);

			if (_isOpened)
				return;

			_isOpened = true;

			foreach (var product in Repository.Instance.OwnedProducts)
			{
				var inventoryElement = Instantiate(_inventoryElementSample, _layout);
				inventoryElement.OnOpen(product);
				Elements.Add(inventoryElement);
			}
		}

		/// <inheritdoc />
		public override void Close()
		{
			base.Close();

			_isOpened = false;

			foreach (var element in Elements)
			{
				element.OnClose();
			}

			Elements.Clear();
		}

		private void OnDisable()
		{
			Close();
		}
	}
}