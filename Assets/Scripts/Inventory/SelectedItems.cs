﻿using System;
using System.Collections.Generic;
using System.Linq;
using Fox3D.ZeusPot.Market;
using UnityEngine;

namespace Fox3D.ZeusPot.Inventory
{
	public sealed class SelectedItems
	{
		private const string FILE_NAME = "selected_items.dat";

		private static SelectedItems _instance;

		public static SelectedItems Instance { get { return _instance = _instance ?? new SelectedItems(); } }

		private SelectedItems()
		{
			if (ES2.Exists(FILE_NAME))
			{
				LoadAll();
				return;
			}

			foreach (var preownedProduct in ProductsList.Instance.PreownedProducts)
				Select(preownedProduct);
		}

		private readonly HashSet<AbstractProduct> _selectedProducts = new HashSet<AbstractProduct>();

		public HashSet<AbstractProduct> SelectedProducts { get { return _selectedProducts; } }

		public event Action<AbstractProduct> OnProductSelect = delegate { };

		public void Select(AbstractProduct product)
		{
			if (!product.IsOwned)
				throw new InvalidOperationException("Selecting not purchased product");

			var prevItem = SelectedProducts.FirstOrDefault(x => x.GetType() == product.GetType());
			
			if (prevItem != null)
			{
				if (prevItem == product)
					return;

				SelectedProducts.Remove(prevItem);
			}

			SelectedProducts.Add(product);
			OnProductSelect(product);

			SaveAll();
		}

		private void LoadAll()
		{
			_selectedProducts.UnionWith(ES2.LoadArray<string>(FILE_NAME).Select(productKey => ProductsList.Instance.Products.First(x => x.Key == productKey)));
		}

		private void SaveAll()
		{
			ES2.Save(_selectedProducts.Select(selectedProduct => selectedProduct.Key).ToArray(), FILE_NAME);
		}
	}
}