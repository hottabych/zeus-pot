﻿using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemy;    //префаб врага
    public Transform[] slots;   //все слоты
    public List<Transform> freeSlots; //незанятые слоты
    public bool flipX;

    private void Awake()
    {
        freeSlots = new List<Transform>();
    }


    //TODO: использовать pooling
    public void Spawn()
    {
        Enemy newEnemy = Instantiate(enemy, transform.position, Quaternion.identity).GetComponent<Enemy>();
        newEnemy.enemySpawner = this;             
    }


    private void CreateFreeSlotsList()
    {
        freeSlots.Clear();
        foreach (var slot in slots)
        {
            if (slot.GetComponent<EnemySlotData>().occupied == false)
            {
                freeSlots.Add(slot);
            }
        }
    }


    public EnemySlotData FindRandomFreeSlot()
    {
        CreateFreeSlotsList();
        if (freeSlots.Count == 0) return null;

        int rnd = Random.Range(0, freeSlots.Count);
        return freeSlots[rnd].GetComponent<EnemySlotData>();
    }
}
