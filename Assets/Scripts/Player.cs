﻿using UnityEngine;
using Spine.Unity;

public class Player : MonoBehaviour
{
    public int health = 100;
    public int shots = 10; //TODO: отказаться от лишней переменной и использовать Stack.Count

    public AudioClip hurtClip;
    public AudioClip shootClip;
    public AudioClip reloadClip;

    new AudioSource audio;

    bool facingRight = true;

    public Enemy clickedEnemy = null;
    public float enemyKillDelay = .4f;    

    [Header("Spine settings")]
    
    [SpineAnimation]
    public string idleAnimation;
    [SpineAnimation]
    public string damageAnimation;
    [SpineAnimation]
    public string defeatAnimation;
    [SpineAnimation]
    public string reloadAnimation;
    [SpineAnimation]
    public string rotateAnimation;
    [SpineAnimation]
    public string shootMidAnimation;
    [SpineAnimation]
    public string shootHiAnimation;
    [SpineAnimation]
    public string shootLoAnimation;

    protected SkeletonAnimation skeletonAnimation;        

    [SpineEvent]
    public string eventShot;


    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    private void Start()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.state.Complete += AnimationCompleteHandler;
        skeletonAnimation.state.Event += ShotEventHanlder;

        GameManager.instance.ui.RefillAmmo();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //ранение
        if (collision.gameObject.tag == "Bullet")
        {
            int rnd1 = Random.Range(0, 100);
            if (rnd1 < 20) audio.PlayOneShot(hurtClip);

            health -= collision.GetComponent<BulletData>().bulletDamage;
            GameManager.instance.ui.healthbar.value = health * 0.01f;
            Destroy(collision.gameObject);

            var currentAnimation = skeletonAnimation.state.GetCurrent(0).animation.name;

            //отображаем ранение только, если он стоит неподвижно, чтобы не перебивать стрельбу
            if (currentAnimation == idleAnimation)
                SetAnimation(damageAnimation, false);

            int rnd2 = Random.Range(0, 100);
            if (rnd2 < 10)
                GameManager.instance.PlayAudio(GameManager.instance.shootQuickerClip, false);
        }
    }

    //вызывается из эвента анимации Rotation
    public void FlipX()
    {
        if (facingRight == false)
        {
            skeletonAnimation.skeleton.FlipX = true;
        }
        else
        {
            skeletonAnimation.skeleton.FlipX = false;
        }
    }

    //вызывается из Enemy.OnMouseDown
    public void Shoot(Enemy enemy = null)
    {
        var currentAnimation = skeletonAnimation.state.GetCurrent(0).animation.name;

        if (facingRight) //если смотрим вправо, а щелкнули слева
        {
            if (GameManager.instance.mouseLastClicked.x < Screen.width / 2f)
            {
                facingRight = false;
                //если сейчас перезарядка, то не перебивать её
                if (currentAnimation == reloadAnimation) AddAnimation(rotateAnimation, false);
                else SetAnimation(rotateAnimation);
            }
        }
        else //если смотрим влево, а щелкнули справа
        {
            if (GameManager.instance.mouseLastClicked.x > Screen.width / 2f)
            {
                facingRight = true;

                if (currentAnimation == reloadAnimation) AddAnimation(rotateAnimation, false);
                else SetAnimation(rotateAnimation);
            }
        }
            
        currentAnimation = skeletonAnimation.state.GetCurrent(0).animation.name;

        bool addAnim =
            currentAnimation == rotateAnimation ||      //не прерывать вращения
            currentAnimation == reloadAnimation ?       //не прерывать перезарядку
            true : false;                               //прерывать другие анимации

        if (!enemy) //учебный враг не передает себя
        {
            if (addAnim) AddAnimation(shootMidAnimation); else SetAnimation(shootMidAnimation);
            Invoke("PlayShootClipWithDelay", .2f);
        }
        else
        {
            //выбираем верхнюю/среднюю/нижнюю анимацию в зависимости от уровня врага
            switch (enemy.mySlot.level)
            {
                case EnemySlotData.Level.High:
                    if (addAnim) AddAnimation(shootHiAnimation); else SetAnimation(shootHiAnimation);
                    break;
                case EnemySlotData.Level.Medium:
                    if (addAnim) AddAnimation(shootMidAnimation); else SetAnimation(shootMidAnimation);
                    break;
                case EnemySlotData.Level.Low:                    
                    if (addAnim) AddAnimation(shootLoAnimation); else SetAnimation(shootLoAnimation);
                    break;
            }

            clickedEnemy = enemy;
        }        

        //после этого срабатывает event анимации выстрела и выполняется KillEnemy()

        //TODO: если event не выполнился, то нужно корректно обработать это!
    }


    void PlayShootClipWithDelay() //для учебного врага
    {
        audio.PlayOneShot(shootClip);
    }


    //из события анимаций Shoot, ShootHigh, ShootLow
    public void KillEnemy()
    {
        if (clickedEnemy)
        {
            audio.PlayOneShot(shootClip);
            clickedEnemy.Die();
            clickedEnemy = null;

            shots--;
            LevelStatisticsCounter.instance.levelTryStats.shotsMade++;
            GameManager.instance.ui.DecrementAmmo();            
        }
    }

    public void Reload()
    {        
        SetAnimation(reloadAnimation);
        shots += 10;
        audio.PlayOneShot(reloadClip);
    }


    //вызывается из GameManager.ClickMiss
    public void ShootMiss()
    {
        if (shots <= 0 || GameManager.instance.levelCompleted || GameManager.gamePaused) return;

        if (skeletonAnimation.state.GetCurrent(0).animation.name == reloadAnimation) return;

        //TODO: если было пополнение здоровья, то тоже как-то заблочить выстрел по карте

        audio.PlayOneShot(shootClip);

        shots--;
        LevelStatisticsCounter.instance.levelTryStats.shotsMade++;
        LevelStatisticsCounter.instance.levelTryStats.shotsMissed++;
        GameManager.instance.ui.DecrementAmmo();

        //TODO: поворот, если тапнули на другой стороне (через facingRight и Screen)
        SetAnimation(shootMidAnimation);
    }


#region Spine Methods

    public void SetAnimation(string name, bool loop = false)
    {        
        skeletonAnimation.state.SetAnimation(0, name, loop);        
    }

    public void AddAnimation(string name, bool loop = false)
    {
        skeletonAnimation.state.AddAnimation(0, name, loop, 0f);
    }


    //внимание: убивает очередь, если использовали AddAnimation, т.к. содержит SetAnimation
    void AnimationCompleteHandler(Spine.TrackEntry entry)
    {
        var currentAnimation = skeletonAnimation.state.GetCurrent(0).animation.name;

        if (currentAnimation == shootHiAnimation    ||
            currentAnimation == shootLoAnimation    ||
            currentAnimation == shootMidAnimation   ||
            currentAnimation == damageAnimation)
        {
            SetAnimation(idleAnimation, loop: true);
        }

            
        if (currentAnimation == reloadAnimation)
        {
            if (skeletonAnimation.state.GetCurrent(0).next == null) //если в очереди после Reload нет анимаций
                SetAnimation(idleAnimation, loop: true); //ставим Idle
        }

        if (currentAnimation == rotateAnimation)
        {
            FlipX();            
        }
    }

    void ShotEventHanlder(Spine.TrackEntry entry, Spine.Event e)
    {
        if (e.data.name == eventShot)
        {
            KillEnemy();
        }
    }

#endregion
}
