﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;


/// <summary>
/// Учебный враг
/// </summary>
public class EnemyTutorial : MonoBehaviour      //TODO: наследовать от Enemy
{    
    public float moveDuration = 1f;

    public EnemySlotData mySlot; //сюда закинуть слот, в который он побежит

    [SpineAnimation]
    public string idleAnimation;
    [SpineAnimation]
    public string walkAnimation;
    [SpineAnimation]
    public string shotAnimation;
    [SpineAnimation]
    public string dieAnimation;

    protected SkeletonAnimation skeletonAnimation;


    IEnumerator Start()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.state.Complete += AnimationCompleteHandler;

        GameManager.instance.enemiesActive++; //сразу добавляем себя в счетчик!

        //занимаем слот        
        mySlot.occupied = true;
        //перемещаемся туда
        Vector3 targetPosition = mySlot.transform.position;
        if (targetPosition != null)
        {
            transform.DOMove(targetPosition, moveDuration);
        }
        yield return new WaitForSeconds(moveDuration);

        skeletonAnimation.state.SetAnimation(0, idleAnimation, true);
    }

    //по клику передаем игроку команду стрелять
    private void OnMouseDown()
    {
        //поздравляем с победой над учебным врагом            
        GameManager.instance.PlayAudio(GameManager.instance.greatContinueClip);
        GameManager.instance.player.Shoot(); //патрон не тратится, т.к. нет захода в KillEnemy
        Invoke("Die", .2f);       
    }


    private void Die()
    {
        GameManager.instance.enemiesActive--;        
        skeletonAnimation.state.SetAnimation(0, dieAnimation, false);
        GetComponent<Collider2D>().enabled = false;
        mySlot.occupied = false; 
    }


    //вызывается по эвенту в конце анимации гибели
    public void SelfDestroy()
    {
        Destroy(gameObject);
    }

    void AnimationCompleteHandler(Spine.TrackEntry entry)
    {
        if (entry.animation.name == dieAnimation)
            SelfDestroy();
    }
}
