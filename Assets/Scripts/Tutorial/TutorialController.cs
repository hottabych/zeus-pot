﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialController : MonoBehaviour
{
    public enum TutorialState { greetings, answerRadio, confirmLocation, beginMission}
    public TutorialState state = TutorialState.greetings;

    [SerializeField] AudioClip greetingsClip;
    [SerializeField] AudioClip tapRedClip;
    [SerializeField] AudioClip veryWellClip;
    [SerializeField] AudioClip tapGreenClip;
    [SerializeField] AudioClip wellDoneSergeantClip;
    [SerializeField] AudioClip enemySpottedClip;
    [SerializeField] AudioClip needLocationClip;
    [SerializeField] AudioClip excellentWorkClip;
    [SerializeField] AudioClip aliensNearClip;
    
    new AudioSource audio;

    public Animator RGButtonsAnimator;
    // порядок: Red, Green, Green, Red

    public Slider loadingBar;

    public void RedButtonHandler()
    {
        audio.Stop();
        switch (state)
        {
            case TutorialState.greetings:
                //убрать плашку
                RGButtonsAnimator.SetBool("Open", false);
                audio.PlayOneShot(veryWellClip); //тут он долго говорит, и предлагает нажать зеленую кнопку
                StartCoroutine(OpenPanelWithDelay(9f));
                state = TutorialState.answerRadio;
                break;

            case TutorialState.answerRadio:
                audio.PlayOneShot(tapGreenClip);
                break;

            case TutorialState.confirmLocation:
                audio.PlayOneShot(needLocationClip);
                break;

            case TutorialState.beginMission:                
                RGButtonsAnimator.SetBool("Open", false);

                //загрузка первого уровня               
                LoadLevel(SceneManager.GetActiveScene().buildIndex + 1);
                break;
        }
    }

    public void GreenButtonHandler()
    {
        audio.Stop();
        switch (state)
        {
            case TutorialState.greetings:
                audio.PlayOneShot(tapRedClip);   //неправильно, нажми красную           
                break;

            case TutorialState.answerRadio:
                RGButtonsAnimator.SetBool("Open", false);
                audio.PlayOneShot(wellDoneSergeantClip); //это после первого нажатия на зеленую

                state = TutorialState.confirmLocation;
                StartCoroutine(AskConfirmLocation(6.2f, 5f));
                break;

            case TutorialState.confirmLocation:
                RGButtonsAnimator.SetBool("Open", false);
                audio.PlayOneShot(excellentWorkClip); //после второго нажатия на зеленую, и просит красную
                StartCoroutine(OpenPanelWithDelay(4.3f));

                state = TutorialState.beginMission;
                break;

            case TutorialState.beginMission:
                audio.PlayOneShot(aliensNearClip);
                break;

        }
    }

    public void ButtonMiss()
    {        
        switch (state)
        {
            case TutorialState.greetings:
                if (!audio.isPlaying)
                {
                    audio.PlayOneShot(greetingsClip);
                }
                break;
            case TutorialState.answerRadio:
                if (!audio.isPlaying)
                {
                    audio.PlayOneShot(veryWellClip);
                }
                break;
            case TutorialState.confirmLocation:
                if (!audio.isPlaying)
                {
                    audio.PlayOneShot(enemySpottedClip);
                }
                break;
            case TutorialState.beginMission:
                if (!audio.isPlaying)
                {
                    audio.PlayOneShot(excellentWorkClip);
                }
                break;
        }
    }

    IEnumerator OpenPanelWithDelay(float delay)
    {
        yield return new WaitForSeconds(delay);
        RGButtonsAnimator.SetBool("Open", true);
    }

    IEnumerator AskConfirmLocation(float phraseDelay, float panelDelay)
    {
        yield return new WaitForSeconds(phraseDelay);
        audio.PlayOneShot(enemySpottedClip);

        yield return new WaitForSeconds(panelDelay);
        RGButtonsAnimator.SetBool("Open", true);
    }


    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    private void Start()
    {
        audio.PlayOneShot(greetingsClip);

        //выкатить плашку
        RGButtonsAnimator.SetBool("Open", true);
    }


    private IEnumerator LoadNextScene(int sceneIndex)
    {
        loadingBar.gameObject.SetActive(true);

        var loading = SceneManager.LoadSceneAsync(sceneIndex);

        while (!loading.isDone)
        {            
            float progress = Mathf.Clamp01(loading.progress / .9f);
            loadingBar.value = progress;
            yield return null;
        }
    }

    public void LoadLevel(int sceneIndex)
    {
        StartCoroutine(LoadNextScene(sceneIndex));
    }
}
