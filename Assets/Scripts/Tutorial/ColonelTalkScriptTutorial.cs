﻿using UnityEngine;
using Spine.Unity;

public class ColonelTalkScriptTutorial : MonoBehaviour
{
    public SkeletonAnimation colonelIcon;
    [SpineAnimation]
    public string colonelSaysAnimation;
    [SpineAnimation]
    public string colonelSilentAnimation;

    bool colonelTalk = true;

    public AudioSource tcAudio;

    private void Start()
    {        
        colonelIcon = GetComponent<SkeletonAnimation>();
    }

    private void Update()
    {
        //анимация иконки генерала в углу
        var colonelIconCurrent = colonelIcon.state.GetCurrent(0).animation.name;

        if (tcAudio.isPlaying)
        {
            if (colonelTalk == false)
            {
                if (colonelIconCurrent == colonelSilentAnimation)
                {
                    colonelIcon.state.SetAnimation(0, colonelSaysAnimation, true);
                    colonelTalk = true;
                }
            }
        }
        else
        {
            if (colonelTalk == true)
            {
                colonelIcon.state.SetAnimation(0, colonelSilentAnimation, true);
                colonelTalk = false;
            }

        }
    }
}
