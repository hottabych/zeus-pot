﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//helper class
//method calls from animation event and throws execution to parent
public class EnemyParentSenderTutorial : MonoBehaviour
{
    EnemyTutorial parent;

    private void Awake()
    {
        parent = GetComponentInParent<EnemyTutorial>();
    }

    public void SelfDestroy()
    {
        parent.SelfDestroy();
    }

}
