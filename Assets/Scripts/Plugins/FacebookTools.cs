﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

namespace Fox3D.ZeusPot.Plugins
{
	/// <summary>
	/// Facebook plugin wrapper and initializer.
	/// </summary>
	public sealed class FacebookTools : MonoBehaviour
	{
		private const string APP_PLAYSTORE_LINK = "";

		#region Share

		private const string SHARE_TOPIC = "ZeusPot";

		private const string SHARE_CONTENT = "Fox3D Game";

		private const string GAME_URI_LINK = "https://www.facebook.com/ZeusPot/";

		private const string LOGO_URI_LINK = "https://scontent.fiev2-1.fna.fbcdn.net/v/t1.0-1/p200x200/20621130_835854993255329_1881389641326068708_n.png?oh=8ee66cbb5c26137d9abd20f85cb0d10a&oe=5A55A909";

		#endregion

		private static FacebookTools _instance;

		/// <summary>
		/// Instance of <see cref="FacebookTools"/>.
		/// </summary>
		public static FacebookTools Instance
		{
			get
			{
				if (_instance != null)
					return _instance;

				_instance = new GameObject("Facebook controller").AddComponent<FacebookTools>();
				_instance.Initialize();

				return _instance;
			}
		}

		public event Action OnInitializationFailed = delegate { };

		private void Awake()
		{
			if (_instance != null && _instance != this)
				Destroy(this); // destroying instance dublicates.
			else if (_instance == null)
				_instance = this;

			Initialize();

			DontDestroyOnLoad(gameObject);
		}

		private void Initialize() // Facebook SDK initialization.
		{
			if (!FB.IsInitialized)
				FB.Init(
					delegate
					{
						if (FB.IsInitialized)
							FB.ActivateApp();
						else
							OnInitializationFailed();
					},
					delegate (bool isGameShown)
					{
						//Time.timeScale = isGameShown ? 1 : 0;

						// TODO: pause game.
					});
			else
				FB.ActivateApp();
		}

		/// <summary>
		/// Loging into facebook account.
		/// </summary>
		public void Login()
		{
			if (!FB.IsInitialized)
				throw new InvalidOperationException("Facebook is not initialized.");

			if (FB.IsLoggedIn)
				return;

			var permissions = new List<string>() { "public_profile", "email", "user_friends" };
			FB.LogInWithReadPermissions(permissions);
		}

		/// <summary>
		/// Loging out from facebook account.
		/// </summary>
		public void Logout()
		{
			if (!FB.IsInitialized)
				throw new InvalidOperationException("Facebook is not initialized.");

			if (!FB.IsLoggedIn)
				throw new InvalidOperationException("You're trying to logout when player is not logged in.");

			FB.LogOut();
		}

		/// <summary>
		/// Sharing the game on facebook.
		/// </summary>
		public void Share()
		{
			FB.ShareLink(new Uri(GAME_URI_LINK), SHARE_TOPIC, SHARE_CONTENT, new Uri(LOGO_URI_LINK));
		}

		/// <summary>
		/// Inviting player to the game.
		/// </summary>
		public void Invite()
		{
			FB.Mobile.AppInvite(new Uri(APP_PLAYSTORE_LINK));
		}

		/// <summary>
		/// Getting list of friends playing this game.
		/// </summary>
		/// <returns>List of friends playing this game.</returns>
		public IList GetFriendsPlayingThisGame()
		{
			var query = "/me/friends";
			var list = new List<object>();

			FB.API(query, HttpMethod.GET, result =>
			{
				var dictionary = (Dictionary<string, object>) Facebook.MiniJSON.Json.Deserialize(result.RawResult);
				list = (List<object>) dictionary["data"];
			});

			return list;
		}
	}
}