﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Fox3D.ZeusPot.Plugins
{
	public sealed class FacebookFriendsListOutput : MonoBehaviour
	{
		[SerializeField]
		private Text _text;

		public void Output()
		{
			_text.text = "Friends list:";

			foreach (var player in FacebookTools.Instance.GetFriendsPlayingThisGame())
				_text.text += "\n" + ((Dictionary<string, object>)player)["name"].ToString();
		}
	}
}