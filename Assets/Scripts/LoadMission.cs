﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

/// <summary>
/// Компонент на кнопке Play Mission.
/// Отвечает за переход с экрана запоминания букв в сцену миссии
/// </summary>
public class LoadMission : MonoBehaviour
{
    public Slider loadingBar;
    public void PlayMissionClick()
    {
        //загрузка следующей сцены
        StartCoroutine(LoadNextScene(SceneManager.GetActiveScene().buildIndex + 1));
    }

    private IEnumerator LoadNextScene(int sceneIndex)
    {
        loadingBar.gameObject.SetActive(true);

        var loading = SceneManager.LoadSceneAsync(sceneIndex);

        while (!loading.isDone)
        {
            float progress = Mathf.Clamp01(loading.progress / .9f);
            loadingBar.value = progress;
            yield return null;
        }
    }
}
