﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Fox3D.ZeusPot.Map
{
	/// <summary>
	/// Road contains <see cref="AbstractRoadElement"/>s to split spline to separate elements.
	/// </summary>
	public sealed class Road : MonoBehaviour
	{
		[SerializeField]
		private BezierSpline _spline;

		[SerializeField]
		private List<AbstractRoadElement> _elements;

		/// <summary>
		/// Road's shape.
		/// </summary>
		public BezierSpline Spline {get { return _spline; } }

		/// <summary>
		/// Road separtors list.
		/// </summary>
		public List<AbstractRoadElement> Elements {get { return _elements; } }


		/// <summary>
		/// Invokes when play interact somehow with element or clicks on it.
		/// </summary>
		public event Action<AbstractRoadElement> OnElementInteract = delegate { };

		private void Awake()
		{
			foreach (var element in Elements)
			{
				element.OnClick += OnElementInteract;
			}
		}

		#region EDITOR FEATURES

#if UNITY_EDITOR

		private void OnDrawGizmos()
		{
			foreach (var element in Elements)
			{
				Gizmos.color = Color.yellow;

				if (element.GetType() == typeof(BuildingRoadElement))
					Gizmos.color = Color.red;
				else if (element.GetType() == typeof(RoadElement))
					Gizmos.color = Color.blue;

				Gizmos.DrawSphere(Spline.GetPoint(element.Position), 0.1f);
			}
		}

		private void OnValidate()
		{
			_elements = Elements.Distinct().ToList();
		}

#endif
		#endregion
	}
}