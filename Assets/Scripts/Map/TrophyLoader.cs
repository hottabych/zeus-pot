﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// показывает нужный символ кубка на карте
/// </summary>
public class TrophyLoader : MonoBehaviour
{
    [HideInInspector]
    public SpriteRenderer cupImage;

    public int level = 2; //buildindex уровня, к которому принадлежит этот кубок

    public Sprite bronzeCup;
    public Sprite silverCup;
    public Sprite goldCup;    


    private void Awake()
    {
        cupImage = GetComponent<SpriteRenderer>();
    }

    private void Start()
    {
        //cupImage.sprite = noCup; //TODO: нужна еще одна картинка "нет кубка" (темный силуэт)
        string loadPath = "stats.dat?tag=" + level + ".cupType";
        LevelStats.CupType loadedCup = LevelStats.CupType.Bronze;
        if (ES2.Exists(loadPath))
        {
            loadedCup = ES2.Load<LevelStats.CupType>(loadPath);
            switch (loadedCup)
            {
                case LevelStats.CupType.Gold:
                    cupImage.sprite = goldCup;
                    break;
                case LevelStats.CupType.Silver:
                    cupImage.sprite = silverCup;
                    break;
                case LevelStats.CupType.Bronze:
                    cupImage.sprite = bronzeCup;
                    break;
            }
        }
    }
}
