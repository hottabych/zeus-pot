﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Fox3D.ZeusPot.Map
{
	/// <summary>
	/// Used by <see cref="Road"/> to split <see cref="BezierSpline"/> to separete elements.
	/// </summary>
	public abstract class AbstractRoadElement : MonoBehaviour, IPointerClickHandler
	{
		[SerializeField]
		private float _position;

		/// <summary>
		/// Road element related to spline position.
		/// </summary>
		public float Position { get { return _position; } }

		/// <summary>
		/// On <see cref="IPointerClickHandler"/> event invoke.
		/// </summary>
		public event Action<AbstractRoadElement> OnClick = delegate { };

		/// <inheritdoc />
		void IPointerClickHandler.OnPointerClick(PointerEventData eventData)
		{
			OnClick(this);
		}

		/// <summary>
		/// On point reach behaviour.
		/// </summary>
		public abstract void OnPointReach();
	}
}