﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Fox3D.ZeusPot.Market.UI
{
	public class SceneLoader : MonoBehaviour
	{
		public void LoadLevel(string name)
		{
            if (name != "")
			    SceneManager.LoadScene(name);
		}

        public void LoadLevel(int buildindex)
        {
            SceneManager.LoadScene(buildindex);
        }
    }
}