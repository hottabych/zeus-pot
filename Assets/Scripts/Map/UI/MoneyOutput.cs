﻿using Fox3D.ZeusPot.Market;
using UnityEngine;
using UnityEngine.UI;

namespace Fox3D.ZeusPot.Map.UI
{
	public sealed class MoneyOutput : MonoBehaviour
	{
		[SerializeField]
		private Text _self;

		private void OnEnable()
		{
			Money.Instance.OnMoneyAmountChange += UpdateText;
			UpdateText(Money.Instance.Amount);
		}

		private void OnDisable()
		{
			Money.Instance.OnMoneyAmountChange -= UpdateText;
		}

		private void UpdateText(int amount)
		{
			_self.text = amount.ToString();
		}
	}
}