﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Fox3D.ZeusPot
{
	public sealed class TitledButton : MonoBehaviour
	{
		[SerializeField]
		private Text _title;

		[SerializeField]
		private Button _self;

		public Text Title { get { return _title; } }

		public Button Self { get { return _self; } }

		public void Initialize(string text, UnityAction action)
		{
			_title.text = text;
			_self.onClick = new Button.ButtonClickedEvent();
			_self.onClick.AddListener(action);
		}
	}
}