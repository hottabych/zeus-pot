﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollListArrow : MonoBehaviour
{
    public ScrollRect scrollRect;

    public void Scroll (float delta)
    {
        scrollRect.horizontalNormalizedPosition += delta;
    }
}
