﻿using UnityEngine;

namespace Fox3D.ZeusPot.Map
{
	public sealed class BuildingRoadElement : AbstractRoadElement
	{
		[SerializeField]
		private int _buildingIndex;

		/// <inheritdoc />
		public override void OnPointReach()
		{
            GameStatisticsCounter.Instance.ShowLevelStatsWindow(_buildingIndex);
		}
	}
}