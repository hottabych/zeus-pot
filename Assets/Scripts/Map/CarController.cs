﻿using System;
using UnityEngine;

// TODO: to fix multiclicking bug

namespace Fox3D.ZeusPot.Map
{
	/// <summary>
	/// In map car movement controller.
	/// </summary>
	public sealed class CarController : MonoBehaviour
	{
		private const float MAX_ERROR = 0.01f;

		#region Properties

		[SerializeField]
		private Road _road;

		[SerializeField]
		private float _speed;

		[SerializeField]
		private AbstractRoadElement _startPosition;

		#endregion

		private bool IsTargetPositive {get{ return _target.Position > _interpolatePosition; } }

		private AbstractRoadElement _currentPosition; // current object's position.

		private AbstractRoadElement _target;

		private float _interpolatePosition;

		/// <summary>
		/// Invokes when car starts moving.
		/// </summary>
		public event Action<AbstractRoadElement> OnMovingStart = delegate { };

		/// <summary>
		/// Invokes when car stops moving.
		/// </summary>
		public event Action<AbstractRoadElement> OnMovingEnd = delegate { };

		private event Action OnReach = delegate { };

		#region Sergey's code.		
		public int currentLevel = 1; //звездочка, к которой подъезжаем

		//вызывать из OnClick звездочки
		//level соответствует buildindex'у
		public void SetCurrentLevel(int level)
		{
			currentLevel = level;
		}

		#endregion

		private void Awake()
		{
			if (_road == null)
				throw new NullReferenceException();

			#region Binding to OnClick event.

			// NOTE - may be not optimal

			foreach (var element in _road.Elements)
			{
				element.OnClick += MoveTo;
			}

			#endregion

			_currentPosition = _startPosition;
			_target = _currentPosition;
			_interpolatePosition = _currentPosition.Position;
			var pos = _road.Spline.GetPoint(_startPosition.Position);
			transform.position = pos;
			transform.LookAt(pos + _road.Spline.GetDirection(_startPosition.Position + MAX_ERROR));
		}


		private void Update()
		{
			if (_target == _currentPosition)
				return;

			var currentPosition = _interpolatePosition;
			var nextPosition = currentPosition + (IsTargetPositive ? 1f : -1f) * Time.deltaTime * _speed;

			if (IsTargetPositive ? nextPosition >= _target.Position : nextPosition <= _target.Position)
			{
				_currentPosition = _target;
				OnMovingEnd(_currentPosition);
				OnReach();
				return;
			}
			
			_interpolatePosition = nextPosition;
			var pos = _road.Spline.GetPoint(_interpolatePosition);
			transform.position = pos;

			transform.LookAt(pos + (_road.Spline.GetDirection(_interpolatePosition + MAX_ERROR) * (IsTargetPositive ? 1f : -1f)));
		}

		public void MoveTo(AbstractRoadElement element)
		{
			if (element == _target)
				return;

			_target = element;
			OnMovingStart(element);
			OnReach = element.OnPointReach;
		}
	}
}