﻿using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// показывает нужный кубок на весь экран
/// </summary>
public class TrophyRoom : MonoBehaviour
{
    public GameObject trophyPanel;
    public Image trophy;  //целевой кубок на панели

    public void Show(GameObject sender)
    {
        trophyPanel.SetActive(true);
        trophy.sprite = sender.GetComponent<TrophyLoader>().cupImage.sprite;
    }
}
