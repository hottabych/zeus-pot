﻿using System.Linq;
using Fox3D.ZeusPot.Inventory;
using Fox3D.ZeusPot.Market;
using UnityEngine;

namespace Fox3D.ZeusPot.Map
{
	/// <summary>
	/// Customizes cars color. Color is providen by selected product.
	/// [CLASS IS TEMPORARY]
	/// </summary>
	public sealed class TemporaryCarCustomizer : MonoBehaviour
	{
		[SerializeField]
		private Material _default;

		[SerializeField]
		private MeshRenderer _renderer;

		private void OnEnable()
		{
			SelectedItems.Instance.OnProductSelect += CustomizeWrap;
			Customize((CarProduct) SelectedItems.Instance.SelectedProducts.First(x => x is CarProduct));
		}

		private void OnDisable()
		{
			SelectedItems.Instance.OnProductSelect -= CustomizeWrap;
		}

		private void CustomizeWrap(AbstractProduct product) { Customize((CarProduct) product); }

		private void Customize(CarProduct carProduct)
		{
			if (carProduct == null)
			{
				_renderer.material = _default;
				return;
			}

			_renderer.material = carProduct.Material;
		}
	}
}