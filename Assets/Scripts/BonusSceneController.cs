﻿using System;
using System.Collections;
using System.Collections.Generic;
using Fox3D.ZeusPot.Market;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;


public class BonusSceneController : MonoBehaviour
{
    //размер всех массивов должен быть одинаков
    public AudioClip[] letterSounds = new AudioClip[20];
    public AudioClip rightAnswerSound;
    public AudioClip wrongAnswerSound;
    new AudioSource audio;
    public AudioSource indicatorAudio; //На канвасе. Отдельный AudioSource, чтобы генерал не болтал во время индикаторов

    public Image[] greenIndicators = new Image[10];
    public Image[] redIndicators = new Image[10];

    public Sprite greenIndicatorSprite;
    public Sprite redIndicatorSprite;

    public List<Button> letters = new List<Button>(20);
    public Transform lettersGroup;

    int score;    
    int correct;
    int currentQuestion = 0;
    int currentRed = 0, currentGreen = 0;
    bool canTalk = true;
    bool canPress = true;
    bool levelFinished = false;    

    [SerializeField] string saveFileName = "save.dat";    
    

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
        foreach (Transform letter in lettersGroup)
        {
            letters.Add(letter.GetComponent<Button>());            
        }
    }

    private void Update()
    {
        if (canTalk)
        {            
            correct = Random.Range(0, 20);
            //Debug.Log("correct answer is " + correct);            
            try
            {
                if (letterSounds[correct] != null)
                    audio.PlayOneShot(letterSounds[correct]);
            }
            catch (IndexOutOfRangeException e)
            {
                Debug.LogError(e.Message + "\nReason: sound array length < letter list length", this);                
            }

            canTalk = false;
        }

        if (currentQuestion >= 10 && !levelFinished)
        {
            levelFinished = true;
            Save();
        }
    }


    public void LetterClickHandler(int answer)
    {
        if (!canPress) return;
        canPress = false;

        if (answer == correct)
        {
            if (rightAnswerSound) indicatorAudio.PlayOneShot(rightAnswerSound);
            greenIndicators[currentGreen].sprite = greenIndicatorSprite;
            currentGreen++;
            score++;            
        }
        else
        {
            if (wrongAnswerSound) indicatorAudio.PlayOneShot(wrongAnswerSound);
            redIndicators[currentRed].sprite = redIndicatorSprite;
            currentRed++;
        }
        currentQuestion++;
        StartCoroutine(WaitForEndOfPhrase(1.5f));
    }

    IEnumerator WaitForEndOfPhrase(float delay)
    {
        yield return new WaitForSeconds(delay);

        RandomizeLetters();
        canTalk = true;
        canPress = true;
    }

    
    void RandomizeLetters()
    {
        foreach (var letter in letters)     //отсоединяем
        {
            letter.transform.SetParent(null);
        }
        letters.Shuffle();                  //перемешиваем
        foreach (var letter in letters)     //присоединяем
        {
            letter.transform.SetParent(lettersGroup);
        }
    }


    void Save()
    {
        string oldScorePath = saveFileName + "?tag=oldscore" + SceneManager.GetActiveScene().buildIndex;
        string currencyPath = saveFileName + "?tag=currency";
        int oldScore;
        int currency;        

        if (ES2.Exists(oldScorePath))   //смотрим предыдущий рекорд
            oldScore = ES2.Load<int>(oldScorePath);
        else oldScore = 0;
        
        int scoreToAdd = score > oldScore ? score - oldScore : 0;
        if (scoreToAdd > 0) //если побили свой рекорд
        {
            if (ES2.Exists(saveFileName))   //добавляем деньги
            {
                currency = ES2.Load<int>(currencyPath);
                currency += scoreToAdd;

				/* Пока добавил вставку для своей системы. Ещё не разобрал что тут к чему. */

	            Money.Instance.Amount += scoreToAdd;
            }
            else
            {
                currency = scoreToAdd;
            }
            ES2.Save(score, oldScorePath);      //сохраняем новый рекорд
            ES2.Save(currency, currencyPath);   //сохраняем деньги
        }
        else
        {
            //no bonus
        }

        SceneManager.LoadScene("map");
    }
}
