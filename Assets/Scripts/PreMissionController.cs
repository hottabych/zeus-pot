﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreMissionController : MonoBehaviour
{
    public AudioClip[] lettersToPlay;
    public float delay = 1.5f;

    new AudioSource audio;

    private void Awake()
    {
        audio = GetComponent<AudioSource>();
    }

    IEnumerator Start ()
    {
        for (int i = 0; i < lettersToPlay.Length; i++)
        {
            yield return new WaitForSeconds(delay);
            audio.PlayOneShot(lettersToPlay[i]);
        }
	}	
}
