﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySlotData : MonoBehaviour
{
    public enum Level
    {
        High,
        Medium,
        Low
    }

    [HideInInspector]
    public bool occupied;

    public Level level;
}
