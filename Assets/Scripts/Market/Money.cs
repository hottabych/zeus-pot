﻿using System;
using UnityEngine;

namespace Fox3D.ZeusPot.Market
{
	/// <summary>
	/// In-game finanace system implementation.
	/// </summary>
	public sealed class Money
	{
		private static string MONEY_FILE_NAME = "market.dat";

		private static Money _instance;

		/// <summary>
		/// Instance of <see cref="Money"/> object.
		/// </summary>
		public static Money Instance { get { return _instance = _instance ?? new Money(); } }

		private Money() { }

		private int _amount;

		private bool _loaded = false;

		/// <summary>
		/// Amount of money.
		/// </summary>
		public int Amount
		{
			get
			{
				if (_loaded)
					return _amount;

				if (ES2.Exists(MONEY_FILE_NAME))
					_amount = ES2.Load<int>(MONEY_FILE_NAME);
				else
					_amount = 0;

				_loaded = true;
				return _amount;
			}
			set
			{
				if (value == _amount)
					return;
				
				_amount = value;
				OnMoneyAmountChange(_amount);
				ES2.Save(_amount, MONEY_FILE_NAME);
			}
		}

		/// <summary>
		/// On money amount change event.
		/// </summary>
		public event Action<int> OnMoneyAmountChange = delegate { };
	}
}