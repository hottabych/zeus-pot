﻿using Fox3D.ZeusPot.Inventory;
using UnityEngine;

namespace Fox3D.ZeusPot.Market
{
	/// <summary>
	/// Car that can be owned in market.
	/// </summary>
	[CreateAssetMenu(fileName = "Car", menuName = "Market Products/Car", order = 10000)]
	public sealed class CarProduct : AbstractProduct
	{
		[SerializeField]
		private Material _material;
		
		public Material Material { get { return _material; } }

		/// <inheritdoc />
		public override bool Select()
		{
			if (!IsOwned)
				return false;
			
			SelectedItems.Instance.Select(this);

			return true;
		}
	}
}