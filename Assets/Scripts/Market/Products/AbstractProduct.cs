﻿using System;
using Fox3D.ZeusPot.Inventory;
using UnityEngine;

// TODO: IAP

namespace Fox3D.ZeusPot.Market
{
	/// <summary>
	/// Good information container.
	/// </summary>
	public abstract class AbstractProduct : ScriptableObject, IComparable
	{
		[SerializeField]
		private string _key;

		[SerializeField]
		private string _name;

		[SerializeField]
		private int _cost;

		[SerializeField]
		private Sprite _icon;

		/// <summary>
		/// Easy save related key.
		/// </summary>
		public string Key { get { return _key; } }

		/// <summary>
		/// Product name.
		/// </summary>
		public string Name { get { return _name; } }

		/// <summary>
		/// Product's cost.
		/// </summary>
		public int Cost { get { return _cost; } }

		/// <summary>
		/// Product's icon.
		/// </summary>
		public Sprite Icon { get { return _icon; } }

		/// <summary>
		/// Is product owned by player.
		/// </summary>
		public bool IsOwned { get { return ProductsList.Instance.PreownedProducts.Contains(this) || Repository.Instance.IsOwned(this); } }

		/// <summary>
		/// On product purchase event.
		/// </summary>
		public event Action OnProductPurchase = delegate { };

		/// <summary>
		/// Buying product.
		/// </summary>
		public bool Buy()
		{
			if (IsOwned)
				return true;

			if (Money.Instance.Amount < Cost)
				return false;

			Money.Instance.Amount -= Cost;
			Repository.Instance.AddOwned(this);
			OnProductPurchase();

			return true;
		}

		/// <summary>
		/// Selecting product in market.
		/// </summary>
		public abstract bool Select();

		/// <inheritdoc />
		public int CompareTo(object obj)
		{
			var compare = (AbstractProduct) obj;

			var nameCompare = String.Compare(compare.GetType().Name, GetType().Name, StringComparison.Ordinal);

			if (nameCompare == 0)
				return Cost == compare.Cost ? 0 : Cost > compare.Cost ? 1 : -1;

			return nameCompare;
		}
	}
}