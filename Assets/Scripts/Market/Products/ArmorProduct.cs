﻿using Fox3D.ZeusPot.Inventory;
using UnityEngine;

namespace Fox3D.ZeusPot.Market
{
	/// <summary>
	/// Car that can be owned in market.
	/// </summary>
	[CreateAssetMenu(fileName = "Armor", menuName = "Market Products/Armor", order = 10000)]
	public sealed class ArmorProduct : AbstractProduct
	{
		/// <inheritdoc />
		public override bool Select()
		{
			if (!IsOwned)
				return false;
			
			SelectedItems.Instance.Select(this);

			return true;
		}
	}
}