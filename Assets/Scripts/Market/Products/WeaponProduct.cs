﻿using Fox3D.ZeusPot.Inventory;
using UnityEngine;

namespace Fox3D.ZeusPot.Market
{
	/// <summary>
	/// Car that can be owned in market.
	/// </summary>
	[CreateAssetMenu(fileName = "Weapon", menuName = "Market Products/Weapon", order = 10000)]
	public sealed class WeaponProduct : AbstractProduct
	{
		/// <inheritdoc />
		public override bool Select()
		{
			if (!IsOwned)
				return false;

			SelectedItems.Instance.Select(this);

			return true;
		}
	}
}