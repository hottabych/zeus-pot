﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace Fox3D.ZeusPot.Market
{
	/// <summary>
	/// List of products that provided in market.
	/// </summary>
	[CreateAssetMenu(fileName = "Products List", menuName = "Market Products/List", order = 10000)]
	public sealed class ProductsList : ScriptableObject
	{
		private static ProductsList _instance;

		/// <summary>
		/// Instance of products list.
		/// </summary>
		public static ProductsList Instance
		{
			get
			{
				if (_instance == null)
					_instance = _instance = Resources.Load<ProductsList>("ProductsList");

				return _instance ?? Resources.FindObjectsOfTypeAll<ProductsList>().FirstOrDefault();
			}
		}

		[SerializeField]
		private List<AbstractProduct> _products;

		[SerializeField]
		private List<AbstractProduct> _preownedProducts; // products that player preowned. see -> SelectedItems

		private bool _isListValidated;

		/// <summary>
		/// List of products from market.
		/// </summary>
		public List<AbstractProduct> Products
		{
			get
			{
				if (_isListValidated)
					return _products;

				_isListValidated = true;
				var isListEmpty = _products == null || _products.Count == 0;

				if (!isListEmpty)
					return _products;

				_products = Resources.FindObjectsOfTypeAll<AbstractProduct>().ToList();
				_products.Sort((x, y) => string.Compare(x.Key, y.Key, StringComparison.Ordinal));

				return _products;
			}
		}

		public List<AbstractProduct> PreownedProducts { get { return _preownedProducts; } }

#if UNITY_EDITOR

		[ContextMenu("Load all products")]
		private void LoadAll()
		{
			_products = Resources.FindObjectsOfTypeAll<AbstractProduct>().ToList();
			_products.Sort((x, y) => string.Compare(x.Key, y.Key, StringComparison.Ordinal));
		}

#endif
	}
}