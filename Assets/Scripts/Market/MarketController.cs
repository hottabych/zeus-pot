﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MarketController : MonoBehaviour
{
    public int startMoney = 10000;

    private void Start()
    {
        PlayerPrefs.SetInt("money_amount", startMoney);
    }
}
