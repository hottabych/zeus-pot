﻿using System.Collections.Generic;
using PlasticBlock.UI.Windows;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Fox3D.ZeusPot.Market.UI
{
	/// <summary>
	/// In market dialog window.
	/// </summary>
	public sealed class DialogWindow : WindowBase
	{
		[SerializeField]
		private Text _textDialog;

		[SerializeField]
		private TitledButton _buttonSample;

		[SerializeField]
		private RectTransform _layout;

		private List<TitledButton> _buttons = new List<TitledButton>();

		/// <inheritdoc />
		public override void Open(params object[] args)
		{
			base.Open(args);

			if (args == null || args.Length == 0)
				return;

			_textDialog.text = (string) args[0];

			foreach (var button in _buttons)
				Destroy(button.gameObject);

			_buttons.Clear();

			if (args.Length < 2 || args[1] == null) return;

			foreach (var buttonData in (IDictionary<string, UnityAction>) args[1])
			{
				var button = Instantiate(_buttonSample, _layout);
				button.Initialize(buttonData.Key, buttonData.Value);
				_buttons.Add(button);
			}
		}
	}
}