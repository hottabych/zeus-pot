﻿// TODO: Improve selection.

using System;
using System.Collections.Generic;
using PlasticBlock.UI.Windows;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace Fox3D.ZeusPot.Market.UI
{
	/// <summary>
	/// Implements good that could be bought in <see cref="MarketWindow"/>.
	/// </summary>
	public sealed class ProductWindowElement : WindowElementBase
	{
		[SerializeField]
		private AbstractProduct _product;

		[SerializeField]
		private Text _name;

		[SerializeField]
		private Image _icon;

		[SerializeField]
		private Button _buy;

		[SerializeField]
		private Text _cost;

		/// <summary>
		/// Setting current element output product.
		/// </summary>
		public void SetProduct(AbstractProduct product)
		{
			if (product == null)
				throw new ArgumentNullException();

			_product = product;

			if (_name == null || _icon == null || _buy == null || _cost == null)
				throw new NullReferenceException();

			_name.text = _product.Name;
			_icon.sprite = _product.Icon;
			_icon.preserveAspect = true;
			_buy.onClick = new Button.ButtonClickedEvent();
			_buy.onClick.AddListener(BuyButtonBehaviour);
			_cost.text = _product.IsOwned ? "Purchased" : _product.Cost.ToString();
		}

		/// <inheritdoc />
		public override void OnOpen(params object[] args) { }

		/// <inheritdoc />
		public override void OnClose() { }
		
		public void BuyButtonBehaviour()
		{
			if (_product.IsOwned)
			{
				WindowsManagerBase.Instance.WindowsController.Open<DialogWindow>("Product selected.", new Dictionary<string, UnityAction>
				{
					{ "Ok", delegate { WindowsManagerBase.Instance.WindowsController.Open<MarketWindow>(); } }
				});
				_product.Select();
				return; // on purchased product click
			}

			var result = _product.Buy();

			if (!result)
			{
				WindowsManagerBase.Instance.WindowsController.Open<DialogWindow>("You have not enough money to buy this item.", new Dictionary<string, UnityAction>
				{
					{ "Ok", delegate { WindowsManagerBase.Instance.WindowsController.Open<MarketWindow>(); } }
				});

				return;
			}

			_cost.text = "Purchased";
		}
	}
}