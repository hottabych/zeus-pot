﻿using PlasticBlock.UI.Windows;
using UnityEngine;
using UnityEngine.UI;

namespace Fox3D.ZeusPot.Market.UI
{
	/// <summary>
	/// Money amount output element.
	/// </summary>
	public sealed class MoneyAmountOutputElement : WindowElementBase
	{
		[SerializeField]
		private Text _amountOutputElement;

		private void OnEnable()
		{
			Money.Instance.OnMoneyAmountChange += UpdateElement;
		}

		private void OnDisable()
		{
			Money.Instance.OnMoneyAmountChange -= UpdateElement;
		}

		/// <inheritdoc />
		public override void OnOpen(params object[] args)
		{
			UpdateElement(Money.Instance.Amount);
		}

		/// <inheritdoc />
		public override void OnClose() { }

		/// <summary>
		/// Updating window element info.
		/// </summary>
		private void UpdateElement(int count)
		{
			_amountOutputElement.text = count.ToString();
		}
	}
}