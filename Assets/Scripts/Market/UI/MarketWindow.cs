﻿using System;
using System.Collections.Generic;
using System.Linq;
using PlasticBlock.UI.Windows;
using UnityEngine;

namespace Fox3D.ZeusPot.Market.UI
{
	/// <summary>
	/// Market's UI implementation. 
	/// </summary>
	public sealed class MarketWindow : CompoundWindowBase
	{
		[SerializeField]
		private ProductWindowElement _sample;

		[SerializeField]
		private Transform _productsLayout;
		
		private List<Preset> _presets;

		private List<ProductWindowElement> _productElements;

		private int _currentPreset = -1;

		/// <inheritdoc />
		protected override void Awake()
		{
			_presets = new List<Preset>();
			var types = new List<Type>();
			
			foreach (var product in ProductsList.Instance.Products)
			{
				var type = product.GetType();

				if (types.Contains(type))
					continue;
				
				types.Add(type);
			}

			foreach (var type in types)
				_presets.Add(new Preset { products = ProductsList.Instance.Products.Where(x => x.GetType() == type).ToArray()});
			
			_productElements = new List<ProductWindowElement>();
			
			ChangePreset(true);

			base.Awake();
		}

		/// <summary>
		/// Changing preset to negative or positive side.
		/// </summary>
		public void ChangePreset(bool positive)
		{
			if (_currentPreset >= _presets.Count - 1)
				_currentPreset = 0;
			else
				_currentPreset++;

			if (_productElements.Any())
				foreach (var element in _productElements)
					Destroy(element.gameObject);
			
			_productElements.Clear();

			foreach (var product in _presets[_currentPreset].products)
			{
				var element = Instantiate(_sample, _productsLayout);
				element.SetProduct(product);
				_productElements.Add(element);
			}
		}

		/// <summary>
		/// Presets of products to output.
		/// </summary>
		[Serializable]
		public struct Preset
		{
			/// <summary>
			/// Products enumerable.
			/// </summary>
			public AbstractProduct[] products;
		}
	}
}