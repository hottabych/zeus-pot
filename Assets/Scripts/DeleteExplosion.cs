﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeleteExplosion : MonoBehaviour
{
    //вызывается из анимационного ивента Explosion
    public void Delete()
    {
        Destroy(gameObject);
    }
}
