﻿using UnityEngine;
using UnityEngine.UI;

public class LevelUIPanel : MonoBehaviour
{   
    public Text levelName;

    public Text tries;
    public Text wins;    
    public Text shotsMade;
    public Text shotsMissed;
    public Text time;
    public Text correctAnswersPercent;
    public Text correctAAnswers;
    public Text correctBAnswers;
    public Text correctCAnswers;

    public Button detailingButton;
}
