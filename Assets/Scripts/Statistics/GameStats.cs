﻿//статистика по игре
public class GameStats
{
    public int levelsOpened;
    public int levelsCompleted;
    public int totalTries;
    public int totalWins;
    public float averageLevelTime;
    public float averageCorrectAnswer;
    public float knowledgeA;
    public float knowledgeB;
    public float knowledgeC;
}
