﻿using UnityEngine;
using UnityEngine.UI;

public class GameStatsUI : MonoBehaviour
{        
    //public GameObject levelStatsWindow;
    //public Text levelWindowTries;
    //public Text levelWindowWins;
    //public Text levelWindowShotsMade;
    //public Text levelWindowShotsMissed;
    //public Text levelWindowLevelTime;
    //public Text levelWindowCorrectAnswers;
    //public Text levelWindowCorrectA;
    //public Text levelWindowCorrectB;
    //public Text levelWindowCorrectC;

    [Space]
    public GameObject gameStatsWindow;
    public Text gameWindowLevelsOpened;
    public Text gameWindowLevelsCompleted;
    public Text gameWindowTotalTries;
    public Text gameWindowTotalWins;
    public Text gameWindowAverageLevelTime;
    public Text gameWindowAverageCorrect;
    public Text gameWindowAverageA;
    public Text gameWindowAverageB;
    public Text gameWindowAverageC;

    [Space]
    public GameObject levelStatsWindow;
    public GameObject levelStatsPanel;
    public Transform levelStatsContent;

    [Space]
    public GameObject detailedStatsWindow;
    public GameObject detailedStatsPanel;   //префаб панели детальной статистики
    public Transform detailedStatsContent;             //контейнер для панелей в списке с прокруткой
}
