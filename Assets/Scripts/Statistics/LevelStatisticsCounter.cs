﻿using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelStatisticsCounter : MonoBehaviour
{
    public static LevelStatisticsCounter instance;

    public LevelStats levelStats; //временный объект для сохранения в конце уровня. Не сериализуется.    
    public LevelTryStats levelTryStats; //временный объект для сохранения 1 попытки
    private int levelNumber;

    private bool stopTimer;

    public string fileName = "stats.dat";
    public string levelName = "";

    private void Awake()
    {
        instance = this;
        levelStats = new LevelStats();
        levelTryStats = new LevelTryStats();
        levelNumber = SceneManager.GetActiveScene().buildIndex;
    }

    private void Start()
    {
        string loadPath = fileName + "?tag=" + levelNumber + ".tries";
        //вычисляем текущую попытку        
        if (ES2.Exists(fileName))
        {
            if (ES2.Exists(loadPath))
                levelStats.tries = ES2.Load<int>(loadPath);
            else levelStats.tries = 0;
        }
        else levelStats.tries = 0;        
        levelTryStats.tryNumber = levelStats.tries;  
        levelStats.tries++;
    }


    private void Update()
    {
        TimeCounter();
    }


    void TimeCounter()
    {
        if (!stopTimer)
            levelTryStats.time += Time.deltaTime;
    }


    void CalculateLevelTryStats()
    {
        levelTryStats.CalculateAnswersPercentage();
    }


    void CalculateLevelStats()
    {
        float sumTime = 0f;
        int sumShotsMade = 0;
        int sumMissed = 0;
        float sumA = 0f, sumB = 0f, sumC = 0f, sumCorrect = 0f;

        string loadPath;

        //загружаем данные из всех попыток и суммируем
        if (ES2.Exists(fileName))
        {            
            for (int i = 0; i < levelStats.tries; i++)
            {
                loadPath = fileName + "?tag=" + levelNumber + "." + i + "."; ;
                sumTime += ES2.Load<float>(loadPath + "time");      
                sumShotsMade += ES2.Load<int>(loadPath + "shotsMade");
                sumMissed += ES2.Load<int>(loadPath + "shotsMissed");
                sumA += ES2.Load<float>(loadPath + "correctAPerc");
                sumB += ES2.Load<float>(loadPath + "correctBPerc");
                sumC += ES2.Load<float>(loadPath + "correctCPerc");
                sumCorrect += ES2.Load<float>(loadPath + "correctAnswersPerc");                
            }

            //вычисляем средние значения по уровню
            levelStats.averageLevelTime = sumTime / (levelStats.tries);
            levelStats.shotsMade = sumShotsMade / (levelStats.tries);
            levelStats.shotsMissed = sumMissed / (levelStats.tries);
            levelStats.correctAPerc = sumA / (levelStats.tries);
            levelStats.correctBPerc = sumB / (levelStats.tries);
            levelStats.correctCPerc = sumC / (levelStats.tries);
            levelStats.correctAnswersPerc = sumCorrect / (levelStats.tries);
        }
        else
        {   //по идее, сюда не будет заходить, потому что файл уже появляется при сохранении первой попытки...

            //если это первая попытка, просто копируем значения
            levelStats.averageLevelTime = levelTryStats.time;
            levelStats.shotsMade = levelTryStats.shotsMade;
            levelStats.shotsMissed = levelTryStats.shotsMissed;
            levelStats.correctAPerc = levelTryStats.correctAPerc;
            levelStats.correctBPerc = levelTryStats.correctBPerc;
            levelStats.correctCPerc = levelTryStats.correctCPerc;
            levelStats.correctAnswersPerc = levelTryStats.correctAnswersPerc;            
        }

        loadPath = fileName + "?tag=" + levelNumber + ".wins";
        if (ES2.Exists(loadPath))
            levelStats.wins = ES2.Load<int>(loadPath);
        if (levelTryStats.win) levelStats.wins++;

        //мы сменили levelStats.cupType в GameManager       
        LevelStats.CupType previousCup = LevelStats.CupType.Bronze;
        loadPath = fileName + "?tag=" + levelNumber + ".cupType";
        if (ES2.Exists(loadPath))
            previousCup = ES2.Load<LevelStats.CupType>(loadPath);
        if (levelStats.cupType >= previousCup) //если он хуже предыдущего достигнутого, то не ставить его
        {
            levelStats.cupType = previousCup; //для сохранения оставим предыдущий
        }
    }


    void SaveLevelTryStats()
    {
        string savePath = fileName + "?tag=" + levelNumber + "." + levelTryStats.tryNumber + ".";
        ES2.Save(levelTryStats.win, savePath + "win");
        ES2.Save(levelTryStats.shotsMade, savePath + "shotsMade");
        ES2.Save(levelTryStats.shotsMissed, savePath + "shotsMissed");
        ES2.Save(levelTryStats.time, savePath + "time");
        ES2.Save(levelTryStats.correctAnswersPerc, savePath + "correctAnswersPerc");
        ES2.Save(levelTryStats.correctAPerc, savePath + "correctAPerc");
        ES2.Save(levelTryStats.correctBPerc, savePath + "correctBPerc");
        ES2.Save(levelTryStats.correctCPerc, savePath + "correctCPerc");        
    }


    void SaveLevelStats()
    {
        string savePath = fileName + "?tag=" + levelNumber + ".";
        ES2.Save(levelStats.levelName, savePath + "levelName");
        ES2.Save(levelStats.tries, savePath + "tries");
        ES2.Save(levelStats.wins, savePath + "wins");
        ES2.Save(levelStats.cupType, savePath + "cupType");
        ES2.Save(levelStats.shotsMade, savePath + "shotsMade");
        ES2.Save(levelStats.shotsMissed, savePath + "shotsMissed");
        ES2.Save(levelStats.averageLevelTime, savePath + "averageLevelTime");
        ES2.Save(levelStats.correctAPerc, savePath + "correctAPerc");
        ES2.Save(levelStats.correctBPerc, savePath + "correctBPerc");
        ES2.Save(levelStats.correctCPerc, savePath + "correctCPerc");
        ES2.Save(levelStats.correctAnswersPerc, savePath + "correctAnswersPerc");
    }


    //void SaveGameStats()
    //{
    //    ES2.Save(5, fileName + "?tag=game.levelsOpened");
    //    ES2.Save(1, fileName + "?tag=game.levelsCompleted");
    //}


    //вызывается в GameManager
    public void Save()
    {
        stopTimer = true;

        CalculateLevelTryStats();
        SaveLevelTryStats();

        CalculateLevelStats();
        SaveLevelStats();

        //SaveGameStats();
    }
}
