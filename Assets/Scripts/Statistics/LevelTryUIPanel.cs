﻿using UnityEngine;
using UnityEngine.UI;

public class LevelTryUIPanel : MonoBehaviour
{
    public Text tryWindowTryNumber;
    public Text tryWindowWin;
    public Text tryWindowShotsMade;
    public Text tryWindowShotsMissed;
    public Text tryWindowTime;
    public Text tryWindowCorrectAnswersPercent;
    public Text tryWindowCorrectAAnswers;
    public Text tryWindowCorrectBAnswers;
    public Text tryWindowCorrectCAnswers;
}
