﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using UnityEditor;

[RequireComponent(typeof(GameStatsUI))]
public class GameStatisticsCounter : MonoBehaviour
{
    private static GameStatisticsCounter instance;
    public static GameStatisticsCounter Instance { get { return instance; } }

    GameStats gameStats;
    GameStatsUI ui;
    //коллекция всех уровней
    Dictionary<int, LevelStats> levels = new Dictionary<int, LevelStats>();    
    public int[] missionIndices;    //тут перечислить buildindex'ы всех сцен

    public string fileName = "stats.dat";


    private void Awake()
    {
        instance = this;
        ui = GetComponent<GameStatsUI>();
        gameStats = new GameStats();

        if (!ui.detailedStatsPanel) Debug.LogError("Add a prefab of UI Panel in inspector");
    }

    private void Start()
    {
        AddLevelsToDictionary();
        CalculateGameStats();
    }

    private void AddLevelsToDictionary()
    {
        LevelStats loadedLevel;        

        foreach (var index in missionIndices)
        {
            loadedLevel = LoadLevelStats(index);
            if (loadedLevel != null) levels.Add(index, loadedLevel);
        }
    }


    //вызывается в методе AddLevelsToDictionary()
    private LevelStats LoadLevelStats(int levelNumber)
    {
        LevelStats level;

        if (!ES2.Exists("stats.dat")) return null;

        string loadPath = fileName + "?tag=" + levelNumber + ".";
        if (!ES2.Exists(loadPath + "tries"))
        {
            Debug.LogWarning("This level not played yet; info is not available");

            level = new LevelStats()
            {
                tries = 0,
                wins = 0,
                shotsMade = 0,
                shotsMissed = 0,
                averageLevelTime = 0f,
                correctAnswersPerc = 0f,
                correctAPerc = 0f,
                correctBPerc = 0f,
                correctCPerc = 0f
            };
            return level;
        }

        level = new LevelStats()
        {
            levelName = ES2.Load<string>(loadPath + "levelName"),
            tries = ES2.Load<int>(loadPath + "tries"),
            wins = ES2.Load<int>(loadPath + "wins"),
            shotsMade = ES2.Load<int>(loadPath + "shotsMade"),
            shotsMissed = ES2.Load<int>(loadPath + "shotsMissed"),
            averageLevelTime = ES2.Load<float>(loadPath + "averageLevelTime"),
            correctAnswersPerc = ES2.Load<float>(loadPath + "correctAnswersPerc"),
            correctAPerc = ES2.Load<float>(loadPath + "correctAPerc"),
            correctBPerc = ES2.Load<float>(loadPath + "correctBPerc"),
            correctCPerc = ES2.Load<float>(loadPath + "correctCPerc")
        };
        return level;
    }

    //перегруженный метод - принимает string
    private LevelStats LoadLevelStats(string levelName)
    {
        if (!ES2.Exists("stats.dat")) return null;

        LevelStats level = new LevelStats();
        //инициализировать ...

        return level;

        //тут проблема, что номер миссии не совпадает с levelNumber, т.к. это buildindex
    }


    private void CalculateGameStats()
    {
        float sumLevelTime = 0f;
        float sumA = 0f, sumB = 0f, sumC = 0f, sumAverageCorrect = 0f;        

        foreach (var level in levels)
        {
            sumLevelTime += level.Value.averageLevelTime;
            sumA += level.Value.correctAPerc;
            sumB += level.Value.correctBPerc;
            sumC += level.Value.correctCPerc;
            sumAverageCorrect += level.Value.correctAnswersPerc;
            gameStats.totalTries += level.Value.tries;
            gameStats.totalWins += level.Value.wins;

            if (level.Value.tries > 0) gameStats.levelsOpened++;
            if (level.Value.wins > 0) gameStats.levelsCompleted++;
        }
        gameStats.averageLevelTime = sumLevelTime / levels.Count;
        gameStats.averageCorrectAnswer = sumAverageCorrect / levels.Count;
        gameStats.knowledgeA = sumA / levels.Count;
        gameStats.knowledgeB = sumB / levels.Count;
        gameStats.knowledgeC = sumC / levels.Count;
    }


    //при клике на звездочку показывается статистика по уровню
    //levelIndex соответствует buildindex
    public void ShowLevelStatsWindow(int levelIndex)
    {
        //ui.levelStatsWindow.SetActive(true);
        //ui.levelWindowTries.text = "Tries: " + levels[levelIndex].tries;
        //ui.levelWindowWins.text = "Wins: " + levels[levelIndex].wins;
        //ui.levelWindowShotsMade.text = "Shots made: " + levels[levelIndex].shotsMade;
        //ui.levelWindowShotsMissed.text = "Shots missed: " + levels[levelIndex].shotsMissed;
        //ui.levelWindowLevelTime.text = "Av.level time: " + levels[levelIndex].averageLevelTime;
        //ui.levelWindowCorrectAnswers.text = "Correct answers: " + levels[levelIndex].correctAnswersPerc + " %";
        //ui.levelWindowCorrectA.text = "Correct A: " + levels[levelIndex].correctAPerc + " %";
        //ui.levelWindowCorrectB.text = "Correct B: " + levels[levelIndex].correctBPerc + " %";
        //ui.levelWindowCorrectC.text = "Correct C: " + levels[levelIndex].correctCPerc + " %";

        //currentLevelStats = levelIndex;
    }


    //при нажатии на "статистика" в окне Game Stats
    public void ShowLevelStats()
    {
        if (ui.levelStatsContent.childCount > 0)
        {
            foreach (Transform item in ui.levelStatsContent)
            {
                Destroy(item.gameObject);
            }
        }

        ui.levelStatsWindow.SetActive(true);
        foreach (var level in levels)
        {
            var newPanel = Instantiate(ui.levelStatsPanel).GetComponent<LevelUIPanel>();
            newPanel.transform.SetParent(ui.levelStatsContent, false);

            newPanel.levelName.text = level.Value.levelName;
            newPanel.tries.text = "Tries: " + level.Value.tries;
            newPanel.wins.text = "Wins: " + level.Value.wins;
            newPanel.shotsMade.text = "Shots made: " + level.Value.shotsMade;
            newPanel.shotsMissed.text = "Shots missed: " + level.Value.shotsMissed;
            newPanel.time.text = "Time: " + level.Value.averageLevelTime;
            newPanel.correctAnswersPercent.text = "Correct answers: " + level.Value.correctAnswersPerc + "%";
            newPanel.correctAAnswers.text = "Correct A: " + level.Value.correctAPerc + "%";
            newPanel.correctBAnswers.text = "Correct B: " + level.Value.correctAPerc + "%";
            newPanel.correctCAnswers.text = "Correct C: " + level.Value.correctAPerc + "%";

            newPanel.detailingButton.onClick.AddListener(() => ShowDetailedLevelStats(level.Key));            
        }
    }


    //при нажатии на "детальная статистика"
    public void ShowDetailedLevelStats(int levelIndex)
    {        
        if (ui.detailedStatsContent.childCount > 0)
        {
            foreach (Transform item in ui.detailedStatsContent)
            {
                Destroy(item.gameObject);
            }
        }

        ui.detailedStatsWindow.SetActive(true);
        for (int i = 0; i < levels[levelIndex].tries; i++)
        {
            var newPanel = Instantiate(ui.detailedStatsPanel).GetComponent<LevelTryUIPanel>();
            newPanel.transform.SetParent(ui.detailedStatsContent, false);            
            var stats = LoadTryStats(levelIndex, i);
            if (stats == null)
            {
                Debug.LogError("can't load try by try stats");
                return;
            }
            newPanel.tryWindowTryNumber.text = "Try: " + i.ToString();
            newPanel.tryWindowWin.text = "Win: " + stats.win.ToString();
            newPanel.tryWindowShotsMade.text = "Shots made: " + stats.shotsMade.ToString();
            newPanel.tryWindowShotsMissed.text = "Shots missed: " + stats.shotsMissed.ToString();
            newPanel.tryWindowTime.text = "Time: " + stats.time.ToString();
            newPanel.tryWindowCorrectAnswersPercent.text = "Correct answers: " + stats.correctAnswersPerc.ToString() + "%";
            newPanel.tryWindowCorrectAAnswers.text = "Correct A: " + stats.correctAPerc.ToString() + "%";
            newPanel.tryWindowCorrectBAnswers.text = "Correct B: " + stats.correctBPerc.ToString() + "%";
            newPanel.tryWindowCorrectCAnswers.text = "Correct C: " + stats.correctCPerc.ToString() + "%";
        }        
    }


    public LevelTryStats LoadTryStats(int levelNumber, int tryNumber)
    {
        if (!ES2.Exists("stats.dat")) return null;
        string loadPath = fileName + "?tag=" + levelNumber + "." + tryNumber + ".";
        LevelTryStats tryStats = new LevelTryStats()
        {
            win = ES2.Load<bool>(loadPath + "win"),
            shotsMade = ES2.Load<int>(loadPath + "shotsMade"),
            shotsMissed = ES2.Load<int>(loadPath + "shotsMissed"),
            time = ES2.Load<float>(loadPath + "time"),
            correctAnswersPerc = ES2.Load<float>(loadPath + "correctAnswersPerc"),
            correctAPerc = ES2.Load<float>(loadPath + "correctAPerc"),
            correctBPerc = ES2.Load<float>(loadPath + "correctBPerc"),
            correctCPerc = ES2.Load<float>(loadPath + "correctCPerc"),
        };
        return tryStats;
    }


    //при клике на кнопку "Статистика" показывается статистика по игре
    public void ShowGameStatsWindow()
    {
        ui.gameStatsWindow.SetActive(true);
        ui.gameWindowLevelsOpened.text = "Levels opened: " + gameStats.levelsOpened;
        ui.gameWindowLevelsCompleted.text = "LevelsCompleted: " + gameStats.levelsCompleted;
        ui.gameWindowTotalTries.text = "TotalTries: " + gameStats.totalTries;
        ui.gameWindowTotalWins.text = "TotalWins: " + gameStats.totalWins;
        ui.gameWindowAverageLevelTime.text = "AverageLevelTime: " + gameStats.averageLevelTime;
        ui.gameWindowAverageCorrect.text = "AverageCorrect: " + gameStats.averageCorrectAnswer;
        ui.gameWindowAverageA.text = "Knowledge A: " + gameStats.knowledgeA;
        ui.gameWindowAverageB.text = "Knowledge B: " + gameStats.knowledgeB;
        ui.gameWindowAverageC.text = "Knowledge C: " + gameStats.knowledgeC;
    }
}
