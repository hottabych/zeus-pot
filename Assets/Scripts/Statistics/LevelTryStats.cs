﻿//статистика по каждой попытке
public class LevelTryStats
{
    //меняются извне
    public int tryNumber; //номер попытки
    public bool win = false;
    public int shotsMade = 0; //сделано выстрелов
    public int shotsMissed = 0; //выстрелов мимо
    public float time = 0f;
    public int radioAppeared = 0; //сколько раз выскакивала рация с выбором
    public int correctAnswersNum;
    public int correctAnswerANum;
    public int correctAnswerBNum;
    public int correctAnswerCNum;
    public int wrongAnswersNum;
    public int wrongAnswerANum;
    public int wrongAnswerBNum;
    public int wrongAnswerCNum;
    //высчитываются в методе класса - не назначать вручную
    public float correctAnswersPerc = 100f;
    public float correctAPerc = 100f;
    public float correctBPerc = 100f;
    public float correctCPerc = 100f;

    public void CalculateAnswersPercentage()
    {
        if (radioAppeared == 0)     //чтобы избежать деления на ноль
        {
            correctAPerc = 100f; correctBPerc = 100f; correctCPerc = 100f; correctAnswersPerc = 100f;
            return;
        }
        // формула процента правильных ответов: X = (correct / (correct+wrong)) * 100, if (correct+wrong != 0)
        if (correctAnswersNum + wrongAnswersNum == 0) correctAnswersPerc = 100f;
        else correctAnswersPerc = (correctAnswersNum / (correctAnswersNum + wrongAnswersNum)) * 100f;
        if (correctAnswerANum + wrongAnswerANum == 0) correctAPerc = 100f;
        else correctAPerc = (correctAnswerANum / (correctAnswerANum + wrongAnswerANum)) * 100f;
        if (correctAnswerBNum + wrongAnswerBNum == 0) correctBPerc = 100f;
        else correctBPerc = (correctAnswerBNum / (correctAnswerBNum + wrongAnswerBNum)) * 100f;
        if (correctAnswerCNum + wrongAnswerCNum == 0) correctCPerc = 100f;
        else correctCPerc = (correctAnswerCNum / (correctAnswerCNum + wrongAnswerCNum)) * 100f;

        //TODO: неверно считается процент; вероятно неверно инкрементируются прав/неправ ответы
    }
}