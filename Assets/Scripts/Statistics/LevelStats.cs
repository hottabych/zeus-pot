﻿//статистика по каждому уровню
//средние значения по всем попыткам
public class LevelStats
{
    public enum CupType { Gold = 0, Silver = 1, Bronze = 2};

    public string levelName = "n/a";    
    public int tries = 0; //число игровых попыток -> по нему определяется, сколько загружать записей в список    
    public int wins = 0; //число попыток, закончившихся победой
    public CupType cupType = CupType.Bronze; //выигранный кубок
    public int shotsMade = 0;
    public int shotsMissed = 0;
    public float averageLevelTime = 0f;
    public float correctAnswersPerc = 100f;
    public float correctAPerc = 100f;
    public float correctBPerc = 100f;
    public float correctCPerc = 100f;
}
