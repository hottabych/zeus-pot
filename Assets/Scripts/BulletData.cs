﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletData : MonoBehaviour
{
    public int bulletDamage = 1;
    public float bulletSpeed = 1f;
    public float bulletTorque = 500f;
}
