﻿using UnityEngine;
using Spine.Unity;

public class ColonelTalkScript : MonoBehaviour
{
    public SkeletonGraphic colonelIcon;
    [SpineAnimation]
    public string colonelSaysAnimation;
    [SpineAnimation]
    public string colonelSilentAnimation;

    bool colonelTalk = true;

    new public AudioSource audio; //источник разговора (на GameManager / Controller)

    private void Start()
    {
        colonelIcon = GetComponent<SkeletonGraphic>();
    }

    private void Update()
    {
        if (!audio) return;


        //анимация иконки генерала в углу
        var colonelIconCurrent = colonelIcon.AnimationState.GetCurrent(0).animation.name;

        if (audio.isPlaying)
        {
            if (colonelTalk == false)
            {
                if (colonelIconCurrent == colonelSilentAnimation)
                {
                    colonelIcon.AnimationState.SetAnimation(0, colonelSaysAnimation, true);
                    colonelTalk = true;
                }
            }
        }
        else
        {
            if (colonelTalk == true)
            {
                colonelIcon.AnimationState.SetAnimation(0, colonelSilentAnimation, true);
                colonelTalk = false;
            }

        }
    }
}
