﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public string fileName = "profile.dat";

    public int age = 3;
    public string password = "";
    public enum Character { White, Asian, Black};
    public Character character;


    private void Start()
    {
        //if (ES2.Exists(fileName))
        //{
        //    age = ES2.Load<int>(fileName + "?tag=age");
        //    password = ES2.Load<string>(fileName + "?tag=password");
        //    character = ES2.Load<Character>(fileName + "?tag=character");
        //}
    }

    public void SetAge(string input)
    {
        age = Convert.ToInt32(input);
    }

    public void SetPassword(string input)
    {
        password = input;
    }

    public void SelectCharacter(int _character)
    {
        character = (Character)_character;
        Debug.Log(character + " selected");
    }

    public void Save()
    {
        ES2.Save(age, fileName + "?tag=age");
        ES2.Save(password, fileName + "?tag=password");
        ES2.Save(character, fileName + "?tag=character");
    }

    public void Play()
    {
        
    }
}
