﻿using System.Collections;
using UnityEngine;
using DG.Tweening;
using Spine.Unity;


[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Collider2D))]
[RequireComponent(typeof(AudioSource))]
public class Enemy : MonoBehaviour
{
    [HideInInspector]
    public EnemySpawner enemySpawner; //спаунер, породивший этого врага

    public float moveDuration = 1f;
    public float shotsDelay = 1.5f;
    public float idleDelay = 1.5f;

    public BulletData bullet; //префаб снаряда
    //Transform gun;
    Vector3 gun;
    Vector3 gun2;

    [HideInInspector]
    public EnemySlotData mySlot;

    public AudioClip shootClip;
    public AudioClip dieClip;

    new AudioSource audio;

    [HideInInspector]
    public bool dying = false;

    [Header("Spine settings")]

    [SpineAnimation]
    public string idleAnimation;
    [SpineAnimation]
    public string walkAnimation;
    [SpineAnimation]
    public string shotAnimation;
    [SpineAnimation]
    public string dieAnimation;

    [SpineEvent]
    public string eventShot;
    [SpineBone]
    public string boneGunName;
    Spine.Bone boneGun;

    [SpineEvent]
    public string eventShot2 = "";
    [SpineBone]
    public string boneGun2Name = "";
    Spine.Bone boneGun2;

    protected SkeletonAnimation skeletonAnimation;


    private void Awake()
    {        
        //gun = transform.Find("Gun");

        audio = GetComponent<AudioSource>();
    }


    IEnumerator Start()
    {
        skeletonAnimation = GetComponent<SkeletonAnimation>();
        skeletonAnimation.state.Complete += AnimationCompleteHandler;
        skeletonAnimation.state.Event += ShotEventHanlder;
        boneGun = skeletonAnimation.skeleton.FindBone(boneGunName);
        boneGun2 = skeletonAnimation.skeleton.FindBone(boneGun2Name);

        //если спаунер повернут - развернуть и пришельца
        if (enemySpawner.flipX)
        {            
            skeletonAnimation.skeleton.flipX = true;
            
            //var newScale = transform.localScale;
            //newScale.x *= -1;
            //transform.localScale = newScale;
        }

        //занимаем слот
        mySlot = enemySpawner.FindRandomFreeSlot();

        //если не нашли слота - остаемся за пределами экрана и раз в секунду чекаем свободный слот
        while (!mySlot)
        {
            yield return new WaitForSeconds(1f);
            mySlot = enemySpawner.FindRandomFreeSlot();
        }

        mySlot.occupied = true;
        //перемещаемся туда
        Vector3 targetPosition = mySlot.transform.position;
        if (targetPosition != null)
        {
            transform.DOMove(targetPosition, moveDuration);
        }
        yield return new WaitForSeconds(moveDuration);        
        
        SetAnimation(idleAnimation, loop: true);
        yield return new WaitForSeconds(idleDelay);
        //если игра на паузе, то не стреляем
        if (!GameManager.gamePaused)
        {
            if (!dying)
                SetAnimation(shotAnimation, loop: true);
        }

    }


    //по клику передаем игроку команду стрелять
    private void OnMouseDown()
    {
        if (GameManager.gamePaused) return;

        //передаем координаты мыши в GameManager
        GameManager.instance.mouseLastClicked = Input.mousePosition;

        if (GameManager.instance.player.shots > 0)
        {
            GameManager.instance.player.Shoot(this);
        }
    }

    //вызывается из Player.KillEnemy
    public void Die()
    {
        dying = true;

        int rnd = Random.Range(0, 100);
        if (rnd < 20) audio.PlayOneShot(dieClip);
        
        SetAnimation(dieAnimation);
        GetComponent<Collider2D>().enabled = false;
        if (mySlot) mySlot.occupied = false;
        GameManager.instance.enemiesActive--;
        GameManager.instance.enemiesTotal--;
        //обновить UI
        GameManager.instance.ui.DeleteTargetFromRadar();

        var rnd2 = Random.Range(0, 100);
        if (rnd2 < 10)
            GameManager.instance.PlayAudio(GameManager.instance.greatContinueClip, false);
        else if (rnd2 < 20)
            GameManager.instance.PlayAudio(GameManager.instance.anotherAlienDestroyedClip, false);
    }


    //вызывается из события анимации стрельбы
    public void Shoot(int weaponNum)
    {
        Rigidbody2D shot = null;

        if (GameManager.gamePaused)
        {
            if (!dying)
            {
                SetAnimation(idleAnimation, true);
                return;
            }
        }

        audio.PlayOneShot(shootClip);
        
        if (weaponNum == 1)
        {
            gun = boneGun.GetWorldPosition(transform);
            //TODO: использовать pooling
            shot = Instantiate(bullet.gameObject, gun, Quaternion.identity).GetComponent<Rigidbody2D>();
        }
        else if (weaponNum == 2)
        {
            if (boneGun2 != null) gun2 = boneGun2.GetWorldPosition(transform);
            shot = Instantiate(bullet.gameObject, gun2, Quaternion.identity).GetComponent<Rigidbody2D>();
        }
        Vector2 shotDir = (GameManager.instance.player.transform.position - transform.position);
        shot.velocity = shotDir * bullet.bulletSpeed;

        //разворачиваем пулю
        if (enemySpawner.flipX)
        {
            var rend = shot.GetComponent<SpriteRenderer>();
            if (rend) rend.flipX = true;
            var skel = shot.GetComponent<SkeletonAnimation>();
            if (skel) skel.skeleton.flipX = true;
        }
    }


    //вызывается по эвенту в конце анимации гибели
    public void SelfDestroy()
    {
        Destroy(gameObject);
    }


    public void SetAnimation(string name, bool loop = false)
    {
         skeletonAnimation.state.SetAnimation(0, name, loop);
    }


    public string GetCurrentStateName()
    {
        return skeletonAnimation.state.GetCurrent(0).animation.name;
    }


    void AnimationCompleteHandler(Spine.TrackEntry entry)
    {
        if (entry.animation.name == dieAnimation)
            SelfDestroy();
    }

    void ShotEventHanlder(Spine.TrackEntry entry, Spine.Event e)
    {
        if (e.data.name == eventShot)
        {            
            Shoot(1);
        }

        if (e.data.name == eventShot2)
        {
            Shoot(2);
        }
    }
}
