﻿#pragma warning disable 618

using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace PlasticBlock.EditorExtensions
{
	/// <summary>
	/// Draws additional information in hierarchy window.
	/// </summary>
	[InitializeOnLoad]
	internal static class HierarchyTools
	{
        private const string MENU_NAME = "Tools/Hierarchy/Toggle additional information %H";

		private const string MENU_NAME_TAGS = "Tools/Hierarchy/Toggle additional information tags";

        private static bool _enabled;

		private static bool _drawTags;

		private static bool DrawTags { get { return _enabled && _drawTags; } }

        /// <summary>
        /// GameObjects from scene.
        /// </summary>
        private static readonly IDictionary<int, GameObject> Objects = new Dictionary<int, GameObject>();

        /// <summary>
        /// Static class constructor that used for initialization.
        /// </summary>
        static HierarchyTools()
	    {
	        _enabled = EditorPrefs.GetBool(MENU_NAME, true); // Saving extension value.
			_drawTags = EditorPrefs.GetBool(MENU_NAME_TAGS, false);
            EditorApplication.hierarchyWindowChanged += UpdateDictionary;
            EditorApplication.hierarchyWindowItemOnGUI += DrawHierarchyElements;
            
            SceneManager.sceneLoaded += OnSceneChanged;

            EditorApplication.delayCall += () => {
				Menu.SetChecked(MENU_NAME, _enabled);
				EditorPrefs.SetBool(MENU_NAME, _enabled);

				Menu.SetChecked(MENU_NAME_TAGS, _drawTags);
				EditorPrefs.SetBool(MENU_NAME_TAGS, _drawTags);
			};
            
            UpdateDictionary();
        }

		/// <summary>
		/// Hiding/unhiding tools menu item setting.
		/// </summary>
        [MenuItem(MENU_NAME)]
	    public static void HideTools()
	    {
			Menu.SetChecked(MENU_NAME, !_enabled);
			EditorPrefs.SetBool(MENU_NAME, !_enabled);
			_enabled = !_enabled;
		}

		/// <summary>
		/// Hiding/unhiding tags.
		/// </summary>
		[MenuItem(MENU_NAME_TAGS)]
		public static void HideTags()
		{
			Menu.SetChecked(MENU_NAME_TAGS, !_drawTags);
			EditorPrefs.SetBool(MENU_NAME_TAGS, !_drawTags);
			_drawTags = !_drawTags;
		}

		private static void OnSceneChanged(Scene scene, LoadSceneMode mode)
	    {
	        UpdateDictionary();
	    }
        
		/// <summary>
		/// Updates dictionary of gameObjects in scene.
		/// </summary>
		private static void UpdateDictionary()
        {
            if (!_enabled)
                return;

            Objects.Clear();

			GameObject[] gameObjects = Resources.FindObjectsOfTypeAll(typeof (GameObject)) as GameObject[];

			if (gameObjects == null) return;
			foreach (GameObject g in gameObjects)
			{
				if (g == null) continue;

				int instanceId = g.GetInstanceID();
				Objects.Add(instanceId, g);
            }
        }

		/// <summary>
		/// Drawing extensions on Hierarchy window.
		/// </summary>
		private static void DrawHierarchyElements(int instanceId, Rect selectionRect)
        {
            if (!_enabled)
                return;

			if (EditorApplication.isPlaying)
				return;

			Rect toggleRect = new Rect(selectionRect);
			toggleRect.x += toggleRect.width - 20;
			toggleRect.width = 18;

			Rect tagLabelPos = toggleRect;
			float width = (float) Screen.width/4;
			tagLabelPos.x += tagLabelPos.width - width - 20;
			tagLabelPos.width = width;

	        if (!Objects.ContainsKey(instanceId)) return;
	        GameObject g = Objects[instanceId];
                
	        EditorGUI.BeginChangeCheck();
	        if (!EditorApplication.isPlaying)
	        {
		        if (g == null)
			        return;

		        // Outputing checkbox.
		        bool active = GUI.Toggle(toggleRect, g.active, new GUIContent());

				if (DrawTags)
					GUI.Label(tagLabelPos, g.tag, EditorStyles.boldLabel); // outputing tag of gameobject (gameObject.tag)

		        if (active != g.active) // enabling/disabling gameObject.
			        g.SetActive(active);
		        else
			        return;

		        EditorUtility.SetDirty(g);
		        if (active)
			        for (int i = 0; i < g.transform.childCount; i++)
			        {
				        g.transform.GetChild(i).gameObject.SetActive(true);
				        EditorUtility.SetDirty(g);
			        }
		        EditorGUI.EndChangeCheck();
	        }
        }
	}
}
