﻿using System;
using UnityEngine;
using UnityEditor;

namespace PlasticBlock.EditorExtensions
{
	/// <summary>
	/// <see cref="PlayerPrefs"/> InEditor editing window.
	/// </summary>
	[InitializeOnLoad]
	internal class PlayerPrefsEditor : EditorWindow
	{
		/// <summary>
		/// Key for saving.
		/// </summary>
		private string _key;

		/// <summary>
		/// Value for saving.
		/// </summary>
		private string _value;

		/// <summary>
		/// Creating new window.
		/// </summary>
		[MenuItem("Tools/Window/PlayerPrefs Editor")]
		public static void EnableWindow()
		{
			PlayerPrefsEditor window = (PlayerPrefsEditor) GetWindow(typeof(PlayerPrefsEditor));
			window.titleContent = new GUIContent("PlayerPrefs", EditorGUIUtility.ObjectContent(null, typeof(GUIText)).image);
		}

		private void OnGUI()
		{
			GUILayout.BeginArea(new Rect(3, 2, Screen.width - 6, 118), EditorStyles.helpBox);
			GUILayout.Label("PlayerPrefs Editor", EditorStyles.boldLabel);
			GUILayout.Space(5f);
			_key = EditorGUILayout.TextField("Key", _key);
			GUILayout.Space(3f);
			_value = EditorGUILayout.TextField("Data", _value);
			GUILayout.Space(5f);
			if (GUILayout.Button("Set Data")) SetData();
			GUILayout.BeginHorizontal();
			if (GUILayout.Button("Delete Key", EditorStyles.miniButtonLeft)) PlayerPrefs.DeleteKey(_key);
			if (GUILayout.Button("Delete All Keys", EditorStyles.miniButtonRight)) PlayerPrefs.DeleteAll();
			GUILayout.EndHorizontal();
			GUILayout.EndArea();
		}

		/// <summary>
		/// Setting data.
		/// </summary>
		private void SetData()
		{
			float floatData;
			int intData;
			if (int.TryParse(_value, out intData)) PlayerPrefs.SetInt(_key, intData);
			else if (float.TryParse(_value, out floatData)) PlayerPrefs.SetFloat(_key, floatData);
			else PlayerPrefs.SetString(_key, _value);
		}
	}
}
