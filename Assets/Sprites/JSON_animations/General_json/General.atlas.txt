
General.png
size: 1024,512
format: RGBA8888
filter: Linear,Linear
repeat: none
Body
  rotate: false
  xy: 276, 196
  size: 272, 276
  orig: 272, 276
  offset: 0, 0
  index: -1
Eye_Closed
  rotate: false
  xy: 276, 73
  size: 96, 16
  orig: 96, 16
  offset: 0, 0
  index: -1
Hat
  rotate: false
  xy: 2, 2
  size: 137, 22
  orig: 137, 22
  offset: 0, 0
  index: -1
Head
  rotate: false
  xy: 824, 316
  size: 162, 156
  orig: 162, 156
  offset: 0, 0
  index: -1
Lanq
  rotate: false
  xy: 824, 292
  size: 56, 22
  orig: 56, 22
  offset: 0, 0
  index: -1
Lip_Down
  rotate: false
  xy: 550, 222
  size: 128, 65
  orig: 131, 68
  offset: 2, 0
  index: -1
Noise1
  rotate: false
  xy: 276, 91
  size: 272, 103
  orig: 272, 103
  offset: 0, 0
  index: -1
Noise2
  rotate: false
  xy: 2, 26
  size: 272, 136
  orig: 272, 136
  offset: 0, 0
  index: -1
Screen
  rotate: false
  xy: 2, 164
  size: 272, 308
  orig: 272, 308
  offset: 0, 0
  index: -1
Toh
  rotate: true
  xy: 988, 394
  size: 78, 28
  orig: 78, 28
  offset: 0, 0
  index: -1
warp1
  rotate: false
  xy: 550, 380
  size: 272, 92
  orig: 272, 92
  offset: 0, 0
  index: -1
warp2
  rotate: false
  xy: 550, 289
  size: 272, 89
  orig: 272, 89
  offset: 0, 0
  index: -1
